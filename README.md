# Ionic Angular Application

This application is purely developed with Ionic Framework and Angular 13.


## Table of Contents
- [Getting Started](#getting-started)


## Getting Started

* [Download the installer](https://nodejs.org/) for Node LTS.
* Install the ionic CLI globally: `npm install -g ionic`
* Clone this repository: `git clone https://gitlab.com/opencontent/stanza-del-cittadino/mobile-app.git`.
* Run `npm install` from the project root.
* Run `ionic serve` in a terminal from the project root.
