package it.ufficigiudiziarideltrentinoaltoadigesudtirol.stanzadelcittadino;

import android.os.Bundle;

import com.equimaps.capacitorblobwriter.BlobWriter;
import com.getcapacitor.BridgeActivity;
import com.whitestein.securestorage.SecureStoragePluginPlugin;

import io.sentry.capacitor.SentryCapacitor;

public class MainActivity extends BridgeActivity {
  @Override
  public void onCreate(Bundle savedInstanceState) {
    registerPlugin(SecureStoragePluginPlugin.class);
    registerPlugin(BlobWriter.class);
    registerPlugin(SentryCapacitor.class);
    super.onCreate(savedInstanceState);
  }
}

