// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  appName: 'Servizi Telematici della Giustizia del Trentino Alto Adige-Südtirol',
  api_stanza: 'https://www2.stanzadelcittadino.it/uffici-giudiziari-del-trentino-alto-adige-sudtirol',
  sentry: 'https://3f77373b82d1432582a4f2427eb6777e@o408094.ingest.sentry.io/6108397',
  metadata: 'https://metadata.opencontent.it',
  last_update: '26/07/2024'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
