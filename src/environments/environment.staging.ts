export const environment = {
  production: true,
  appName: 'Servizi Telematici della Giustizia del Trentino Alto Adige-Südtirol',
  api_stanza: 'https://servizi.comune-qa.bugliano.pi.it/lang',
  sentry: 'https://3f77373b82d1432582a4f2427eb6777e@o408094.ingest.sentry.io/6108397',
  metadata: 'https://metadata.opencontent.it',
  last_update: '26/07/2024'
};
