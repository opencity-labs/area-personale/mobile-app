import moment from 'moment';

export const getDateCalendar = (date: string, locale: string) => {
  const cardCalendar = {
    slot: '',
    letteralDaySelected: '',
  };
  const splitValueCalendar = date.split(/@|\(/);
  cardCalendar.slot = splitValueCalendar[1];
  cardCalendar.letteralDaySelected = moment(splitValueCalendar[0].trim(), 'DD/MM/YYYY')
    .locale(locale)
    .format('DD MMM YY')
    .toString();
  return cardCalendar;
};
