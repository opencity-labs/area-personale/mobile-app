const unflatten = require('unflatten');

export const unflattenObject = (obj) => {
  return unflatten(obj);
};
