import _ from 'lodash';

export const groupArrayOfObjects = (list, key) => {
  return list.reduce((rv, x) => {
    (rv[x[key]] = rv[x[key]] || []).push(x);
    return rv;
  }, {});
};

export const concatGroups = (services: any[], categories: any[], groups: any[]) => {
  services.forEach((el) => {
    el.category = _.filter(categories, (cat) => cat.id === el.topics_id)[0];
  });

  let filterTopics: any = _.filter(services, 'topics_id');

  filterTopics = _.groupBy(services, (item) => {
    if (item.category) return item.category.id;
  });

  const grouped = _.forEach(filterTopics, (value, key) => {
    filterTopics[key] = _.groupBy(filterTopics[key], (item) => {
      return item.service_group_id;
    });
  });

  if (!_.isEmpty(grouped)) {
    for (const [key, value] of Object.entries(grouped)) {
      Object.entries(value).forEach(([k, v]) => {
        if (k !== 'null') {
          const nameGroup = groups.filter((g) => g.id === k)[0];
          value[nameGroup.name] = v;
          value[k].show = searchGroups(v);
          delete value[k];
          return value;
        } else {
          value[k].show = true;
          return value[k];
        }
      });
      return value;
    }
  } else {
    return [];
  }
};

function searchGroups(v: any) {
  return v.some((el) => el.shared_with_group && el.shared_with_group === false);
}
