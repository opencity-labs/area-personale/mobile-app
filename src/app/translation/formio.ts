export let OptionsFormio = {
  buttonSettings: {
    showCancel: true,
    showPrevious: true,
    showNext: true,
    showSubmit: true,
  },
  i18n: {},
};
