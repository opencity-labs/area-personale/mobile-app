import { Pipe, PipeTransform } from '@angular/core';
import { StorageService } from '../services/storage.service';
import Dictionary from '../utility/dictionary';

@Pipe({
  name: 'getCategory',
})
export class GetCategoryPipe implements PipeTransform {
  private dict: Dictionary;
  constructor(private storageService: StorageService) {
    this.dict = new Dictionary();
    this.storageService
      .get('CATEGORIES')
      .then((res) => {
        const category = JSON.parse(res.value);
        category.forEach((el) => {
          this.dict.set(el.id, el.name);
        });
      })
      .catch((err) => {
        console.error(err);
      });
  }

  transform(v: string): any {
    return this.dict.get(v);
  }
}
