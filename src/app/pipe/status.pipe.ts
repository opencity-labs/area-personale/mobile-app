import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Pipe({
  name: 'statusName',
})
export class StatusNamePipe implements PipeTransform {
  constructor(private translateService: TranslateService) {}

  transform(v: string): string {
    let status = '';

    switch (v) {
      case '1000':
        status = this.translateService.instant('stati.bozza');
        break;
      case '1500':
        status = this.translateService.instant('stati.in_attesa_di_pagamento');
        break;
      case '1510':
        status = this.translateService.instant('stati.in_attesa_esito_pagamento');
        break;
      case '1520':
        status = this.translateService.instant('stati.pagamento_successo');
        break;
      case '1530':
        status = this.translateService.instant('stati.pagamento_rifiutato');
        break;
      case '1900':
        status = this.translateService.instant('stati.inviata');
        break;
      case '2000':
        status = this.translateService.instant('stati.acquisita');
        break;
      case '3000':
        status = this.translateService.instant('stati.protocollata');
        break;
      case '4000':
        status = this.translateService.instant('stati.presa_in_carico');
        break;
      case '5000':
        status = this.translateService.instant('stati.in_elaborazione');
        break;
      case '6000':
        status = this.translateService.instant('stati.elaborata');
        break;
      case '7000':
        status = this.translateService.instant('stati.accettata');
        break;
      case '8000':
        status = this.translateService.instant('stati.rifiutata_in_attesa_esito');
        break;
      case '9000':
        status = this.translateService.instant('stati.rifiutata');
        break;
      case '20000':
        status = this.translateService.instant('stati.ritirata');
        break;
      case '50000':
        status = this.translateService.instant('stati.annullata');
        break;
      case '4100':
        status = this.translateService.instant('stati.richiesta_integrazioni');
        break;
      case '4100_legacy':
        status = this.translateService.instant('stati.richiesta_integrazioni_protocollazione');
        break;
      case '4200':
        status = this.translateService.instant('stati.in_attesa_integrazioni');
        break;
      case '4300':
        status = this.translateService.instant('stati.integrazioni_concluse');
        break;
      case '4300_legacy':
        status = this.translateService.instant('stati.integrazioni_in_attesa_protocollate');
        break;
      case '4400':
        status = this.translateService.instant('stati.integrazioni_protocollate');
        break;
      case '4500':
        status = this.translateService.instant('stati.presa_in_carico_integrazioni');
        break;
      default:
        status = '--';
    }
    return status;
  }
}
