import { StatusNamePipe } from './status.pipe';
import { UnslugifyPipe } from './unslugify.pipe';
import { SecurePipe } from './secure.pipe';
import { SanitizeHtmlPipe } from './sanitize-html.pipe';
import { NgModule } from '@angular/core';
import { GetCategoryPipe } from './get-category.pipe';
import { FilterServicesPipe } from './filter-services.pipe';
import { LocalizedDatePipe } from './localizated-date.pipe';

@NgModule({
  imports: [
    // dep modules
  ],
  declarations: [
    StatusNamePipe,
    UnslugifyPipe,
    SecurePipe,
    SanitizeHtmlPipe,
    GetCategoryPipe,
    GetCategoryPipe,
    FilterServicesPipe,
    LocalizedDatePipe,
  ],
  exports: [
    StatusNamePipe,
    UnslugifyPipe,
    SecurePipe,
    SanitizeHtmlPipe,
    GetCategoryPipe,
    FilterServicesPipe,
    LocalizedDatePipe,
  ],
})
export class SharedPipesModule {}
