import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Pipe({
  name: 'filterServices',
})
export class FilterServicesPipe implements PipeTransform {
  constructor(private translateService: TranslateService) {}

  transform(v: string): string {
    let status = '';

    switch (v) {
      case 'topics_id':
        status = this.translateService.instant('common.categorie');
        break;
      case 'recipient_id':
        status = this.translateService.instant('common.destinatari');
        break;
      case 'geographic_area_id':
        status = this.translateService.instant('common.area_geografica');
        break;
      default:
        status = '--';
    }
    return status;
  }
}
