export interface User {
  nome?: string;
  id?: string;
  cognome?: string;
  codice_fiscale?: string;
  data_nascita?: Date;
  luogo_nascita?: string;
  codice_nascita?: string;
  provincia_nascita?: string;
  stato_nascita?: string;
  sesso?: string;
  telefono?: string;
  cellulare?: string;
  email?: string;
  indirizzo_domicilio?: string;
  cap_domicilio?: string;
  citta_domicilio?: string;
  provincia_domicilio?: string;
  stato_domicilio?: string;
  indirizzo_residenza?: string;
  cap_residenza?: string;
  citta_residenza?: string;
  provincia_residenza?: string;
  stato_residenza?: string;
}
