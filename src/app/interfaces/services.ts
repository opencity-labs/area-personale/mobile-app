interface Parameters {
  formio_id: string;
  url: string;
}

interface FlowStep {
  identifier: string;
  title?: any;
  type: string;
  description?: any;
  guide?: any;
  parameters: Parameters;
}

interface ProtocolloParameters {}

interface Integrations {
  trigger: number;
  action: string;
}

export interface ServiceConfig {
  id: string;
  name: string;
  slug: string;
  tenant: string;
  topics: string;
  description: string;
  howto: string;
  who: string;
  special_cases: string;
  more_info: string;
  compilation_info: string;
  final_indications: string;
  coverage: string[];
  response_type?: any;
  flow_steps: FlowStep[];
  protocol_required: boolean;
  protocol_handler?: any;
  protocollo_parameters: ProtocolloParameters;
  payment_required: number;
  payment_parameters: any[];
  integrations: Integrations;
  sticky: boolean;
  status: number;
  access_level: number;
  login_suggested: boolean;
  scheduled_from?: any;
  scheduled_to?: any;
  service_group: string;
  service_group_id: string;
  shared_with_group: boolean;
  allow_reopening: boolean;
  allow_withdraw: boolean;
  workflow: number;
}

export interface ApplicationService {
  user: string;
  service: string;
  data: any;
  protocol_folder_number?: string;
  protocol_folder_code?: string;
  protocol_number?: string;
  protocol_document_id?: string;
  protocolled_at?: string;
  outcome?: boolean;
  outcome_motivation?: string;
  outcome_protocol_number?: string;
  outcome_protocol_document_id?: string;
  outcome_protocolled_at?: string;
  payment_type?: string;
  payment_data?: any;
  status?: string;
}
