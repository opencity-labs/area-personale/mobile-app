export interface Categories {
  parent_id: string;
  id: string;
  name: string;
  slug: string;
  description: string;
  groups?: any;
}
