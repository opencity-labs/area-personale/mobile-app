interface Parameter {
  offset: number;
  limit: number;
}

interface Meta {
  parameter: Parameter;
  count: number;
}

interface Links {
  self: string;
  prev?: any;
  next: string;
}

interface Data {
  'applicant.data.email_address': string;
  'applicant.data.fiscal_code.data.fiscal_code': string;
  'applicant.data.completename.data.name': string;
  'applicant.data.completename.data.surname': string;
  'applicant.data.gender.data.gender': string;
  'applicant.data.Born.data.natoAIl': Date;
  'applicant.data.Born.data.place_of_birth': string;
  'applicant.data.address.data.address': string;
  'applicant.data.address.data.house_number': string;
  'applicant.data.address.data.municipality': string;
  'applicant.data.address.data.postal_code': string;
  'applicant.data.address.data.county': string;
  calendar: string;
}

interface CompiledModule {
  id: string;
  name: string;
  url: string;
  originalName: string;
  description: string;
  created_at: Date;
}

interface OutcomeFile {
  id: string;
  name: string;
  url: string;
  originalName: string;
  description: string;
  created_at: Date;
}

interface Authentication {
  authentication_method: string;
  session_id: string;
  spid_code: string;
  spid_level?: any;
  certificate_issuer?: any;
  certificate_subject?: any;
  certificate?: any;
  instant?: Date;
  session_index: string;
}

export interface ApplicationDataConfig {
  parse_data_calendar?: { slot: string; letteralDaySelected: string };
  data_service?: any;
  id?: string;
  user: string;
  user_name: string;
  service: string;
  service_id: string;
  service_name: string;
  tenant: string;
  subject?: any;
  data: Data;
  compiled_modules: CompiledModule[];
  attachments: any[];
  creation_time: number;
  created_at: Date;
  submission_time: number;
  submitted_at: Date;
  latest_status_change_time: number;
  latest_status_change_at: Date;
  protocol_folder_number?: any;
  protocol_folder_code?: any;
  protocol_number?: any;
  protocol_document_id?: any;
  protocol_numbers: any[];
  protocol_time?: any;
  protocolled_at?: any;
  outcome?: boolean;
  outcome_motivation: string;
  outcome_file: OutcomeFile;
  outcome_attachments: any[];
  outcome_protocol_number?: any;
  outcome_protocol_document_id?: any;
  outcome_protocol_numbers: any[];
  outcome_protocol_time?: any;
  outcome_protocolled_at?: any;
  payment_type?: any;
  payment_data: any[];
  status: string;
  status_name: string;
  authentication: Authentication;
  links: any;
  meetings: any[];
  event_id?: any;
  event_created_at?: any;
  event_version?: any;
  backoffice_data?: any;
}

export interface ApplicationsConfig {
  meta: Meta;
  links: Links;
  data: ApplicationDataConfig[];
}

export interface Attachment {
  id: string;
  name: string;
  mime_type: string;
  file: string;
  url: string;
}

export interface Messages {
  id: string;
  message: string;
  subject: string;
  author: string;
  application: string;
  visibility: string;
  email: string;
  created_at: Date;
  sent_at: Date;
  read_at: Date;
  clicked_at: Date;
  attachments: Attachment[];
  protocol_required: boolean;
  protocolled_at: Date;
  protocol_number: string;
}
