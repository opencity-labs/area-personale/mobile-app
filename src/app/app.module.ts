import { ErrorHandler, Injector, LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Router, RouteReuseStrategy } from '@angular/router';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { AuthModule } from './auth/auth.module';

import { ServicesService } from './services/services.service';
import { StorageService } from './services/storage.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RequestInterceptor } from './interception/request.interceptor';
import { ErrorInterceptor } from './interception/error.interceptor';
import { AuthService } from './auth/auth.service';
import localeIt from '@angular/common/locales/it';
import { registerLocaleData } from '@angular/common';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { FormioAuthConfig } from 'angular-formio/auth';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormioAppConfig, FormioModule } from 'angular-formio';
import { pageTransition } from './animations/page-transition';
import { NgCalendarModule } from 'ionic2-calendar';
import { CustomCalendarComponent } from './components/formio/calendar/custom-calendar.component';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { IonicStorageModule } from '@ionic/storage-angular';
import { Drivers } from '@ionic/storage';
import { FormPage } from './pages/form/form.page';
import { CircularStepperComponent } from './components/circular-stepper/circular-stepper.component';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { registerCalendarComponent } from './components/formio/calendar/custom-calendar.formio';
import { registerPageBreakComponent } from './components/formio/pagebreak/pagebreak.formio';
import { FormioSummaryPage } from './pages/formio-summary/formio-summary.page';
import { PraticeFormPage } from './pages/pratice-form/pratice-form.page';
import { AppConfig, AuthConfig } from './formioConfig/FormioAppConfig';
import { IntroGuard } from './guards/intro.guard';
import { AutoLoginGuard } from './guards/auto-login.guard';
import { CategoriesService } from './services/categories.service';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import * as Sentry from '@sentry/angular';
import { Integrations as TracingIntegrations } from '@sentry/tracing';
import { RecipientsService } from './services/recipients.service';
import { GeographicAreasService } from './services/geographic-areas.service';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { UpdateService } from './services/update.service';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { LockedPageModule } from './pages/locked/locked.module';
import { TabsPageModule } from './pages/tabs/tabs.module';
import { ProfilePageModule } from './pages/profile/profile.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import localeDe from '@angular/common/locales/de';
import { SwiperModule } from 'swiper/angular';

const project = require('package.json');

if (environment.production) {
  Sentry.init({
    dsn: environment.sentry,
    release: `app-tribunali@${project.version}`,
    dist: '43',
    // Set tracesSampleRate to 1.0 to capture 100% of transactions for performance monitoring.
    // We recommend adjusting this value in production.
    tracesSampleRate: 1.0,
    integrations: [
      new TracingIntegrations.BrowserTracing({
        tracingOrigins: [
          'localhost',
          /^\//,
          'capacitor://localhost',
          'ionic://localhost',
          'http://localhost',
          'http://localhost:8080',
          'http://localhost:8100',
        ],
      }),
    ],
  });
}

@NgModule({
    declarations: [
        AppComponent,
        FormPage,
        CircularStepperComponent,
        CustomCalendarComponent,
        FormioSummaryPage,
        PraticeFormPage,
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot({
            backButtonText: '',
            backButtonIcon: 'arrow-back',
            mode: 'ios',
            navAnimation: pageTransition,
        }),
        AppRoutingModule,
        HttpClientModule,
        AuthModule,
        FormsModule,
        ReactiveFormsModule,
        FormioModule,
        ServiceWorkerModule.register('ngsw-worker.js', {
            enabled: environment.production,
            // Register the ServiceWorker as soon as the app is stable
            // or after 30 seconds (whichever comes first).
            registrationStrategy: 'registerWhenStable:30000',
        }),
        NgCalendarModule,
        IonicStorageModule.forRoot({
            name: '__mydb',
            driverOrder: [Drivers.SecureStorage, Drivers.IndexedDB, Drivers.LocalStorage],
        }),
        RoundProgressModule,
        BrowserAnimationsModule,
        TabsPageModule,
        ProfilePageModule,
        LockedPageModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient],
            },
        }),
      SwiperModule
    ],
    providers: [
        InAppBrowser,
        SplashScreen,
        StorageService,
        ServicesService,
        { provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true },
        { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorInterceptor,
            multi: true,
            deps: [AuthService, StorageService, Router],
        },
        AuthService,
        { provide: LOCALE_ID, useValue: 'it-IT' },
        { provide: FormioAuthConfig, useValue: AuthConfig },
        { provide: FormioAppConfig, useValue: AppConfig },
        IntroGuard,
        AutoLoginGuard,
        CategoriesService,
        RecipientsService,
        GeographicAreasService,
        FileOpener,
        {
            provide: ErrorHandler,
            useValue: Sentry.createErrorHandler({
            showDialog: false,
          }),
        },
        AppVersion,
        UpdateService,
        FingerprintAIO,
    ],
    bootstrap: [AppComponent],
    exports: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
  constructor(injector: Injector) {
    registerPageBreakComponent(injector);
    registerCalendarComponent(injector);
    registerLocaleData(localeDe, 'de');
    registerLocaleData(localeIt, 'it');
  }
  get version() {
    return `${project.version}`;
  }

}

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
