import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { FormioCustomComponent, FormioEvent } from 'angular-formio';
import { formatDate } from '@angular/common';
import { CalendarComponent } from 'ionic2-calendar';
import { CalendarMode, Step } from 'ionic2-calendar';
import { CalModalPage } from 'src/app/modals/cal-modal/cal-modal.page';
import { CalendarService } from '../../../services/calendar.service';
import moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { IEvent } from 'ionic2-calendar/calendar.interface';

@Component({
  selector: 'app-calendar',
  templateUrl: './custom-calendar.component.html',
  styleUrls: ['./custom-calendar.component.scss'],
})
export class CustomCalendarComponent implements FormioCustomComponent<number>, OnInit, AfterViewInit, OnDestroy {
  @ViewChild(CalendarComponent) myCal: CalendarComponent;

  public _value: any;
  @Input()
  public set value(v: any) {
    this._value = v;
  }
  public get value(): any {
    return this._value;
  }

  @Output()
  valueChange = new EventEmitter<any>();

  @Output()
  timeLeft = new EventEmitter<number>();

  @Input()
  disabled: boolean;

  @Input() placeholder: string;

  @Input() calendarId: string;
  @Input() select_opening_hours: boolean;
  @Input() overlaps_validation: number;

  @Input() label: string;
  @Input() key: string;
  @Input() fieldOptions: any;
  @Input() opening_hours: any;

  isLoading = false;
  private unsubscribe$ = new Subject<void>();

  calendar = {
    mode: 'month' as CalendarMode,
    step: 30 as Step,
    locale: this.translateService.getDefaultLang() ? this.translateService.getDefaultLang() : 'it',
    currentDate: new Date(),
    allDayLabel: this.translateService.instant('calendar.tutto_il_giorno'),
    dateFormatter: {
      formatMonthViewDay: (date: Date) => {
        return date.getDate().toString();
      },
    },
  };
  formioEvent?: EventEmitter<FormioEvent>;
  selectedDate: Date;
  eventSource: IEvent[] = [];
  availabilitiesData = [];
  viewTitle: string;
  cardCalendar = {
    meetingiD: null,
    slot: null,
    letteralDaySelected: null,
  };

  constructor(
    private alertCtrl: AlertController,
    private modalCtrl: ModalController,
    private calendarServices: CalendarService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.isLoading = true;
    if (!this.calendarId) {
      setTimeout(() => {
        if (this._value) {
          this.setCardCalendar();
        }
        if (this.calendarId && this.availabilitiesData.length === 0) {
          this.calendarServices.getCalendarAvailabilities(this.calendarId).subscribe(
            (res) => {
              this.availabilitiesData = res;
            },
            () => {},
            () => {
              const events = [];
              if (this.availabilitiesData.length > 0) {
                this.availabilitiesData.forEach((el) => {
                  const dateStart = new Date(el.date);
                  const dateEnd = new Date(el.date);
                  dateStart.setHours(0, 0, 1, 0);
                  dateEnd.setHours(0, 1, 0, 0);
                  events.push({
                    title: 'event',
                    startTime: dateStart,
                    endTime: dateEnd,
                    allDay: false,
                  });
                });
                this.eventSource = events;
                this.isLoading = false;
              }
            }
          );
        }
      }, 1500);
    }
  }

  // Change current month/week/day
  next() {
    this.myCal.slideNext();
  }

  back() {
    this.myCal.slidePrev();
  }

  // Selected date reange and hence title changed
  onViewTitleChanged(title) {
    this.viewTitle = title;
  }

  // Calendar event was clicked
  async onEventSelected(event) {
    // Use Angular date pipe for conversion
    const start = formatDate(event.startTime, 'medium', this.calendar.locale);
    const end = formatDate(event.endTime, 'medium', this.calendar.locale);

    const alert = await this.alertCtrl.create({
      header: event.title,
      subHeader: event.desc,
      message: 'From: ' + start + '<br><br>To: ' + end,
      buttons: ['OK'],
    });
    alert.present();
  }

  markDisabled = (date: Date) => {
    return moment(date).isBefore(moment().add(-1, 'days'));
  };

  async openCalModal(timeSelected: any) {
    const modal = await this.modalCtrl.create({
      component: CalModalPage,
      cssClass: 'cal-modal',
      backdropDismiss: false,
      componentProps: {
        timeSelected,
        calendarId: this.calendarId,
        openingHours: this.opening_hours && this.opening_hours.length ? this.opening_hours[0] : null
      },
    });

    await modal.present();

    modal.onDidDismiss().then((result) => {
      if (result.data && result.data.event) {
        this.cardCalendar.slot = result.data.event.start_time + '-' + result.data.event.end_time;
        const data = {
          date: result.data.event.date,
          slot: this.cardCalendar.slot,
          calendar: this.calendarId,
          opening_hour: result.data.event.opening_hour,
        };
        this.calendarServices.createDraftCalendarMeeting(data).subscribe(
          async (res) => {
            this.cardCalendar.meetingiD = res.id;
            this.cardCalendar.letteralDaySelected = moment(result.data.event.date)
              .locale(this.calendar.locale)
              .format('LL')
              .toString();
          },
          (error) => {},
          () => {
            const event = result.data.event;
            const eventValue =
              moment(event.date).format('DD/MM/YYYY').toString() +
              ' @ ' +
              event.start_time +
              '-' +
              event.end_time +
              ' (' +
              this.calendarId +
              '#' +
              this.cardCalendar.meetingiD +
              '#' +
              event.opening_hour +
              ')';
            this.updateValue(eventValue);
          }
        );
      }
    });
  }

  onCurrentDateChanged($event: Date) {}

  updateValue(payload: any) {
    this.value = payload; // Should be updated first
    this.valueChange.emit(payload); // Should be called after this.value update
  }

  ngAfterViewInit(): void {}

  onTimeSelected(ev) {
    if (ev.events !== undefined && ev.events.length !== 0 && !ev.disabled) {
      this.openCalModal(ev);
    }
  }

  resetCalendar() {
    this.updateValue(null);
    this.cardCalendar.meetingiD = null;
    this.cardCalendar.slot = null;
    this.cardCalendar.letteralDaySelected = null;
  }

  setCardCalendar() {
    const splitValueCalendar = this._value.split(/@|\(/);
    this.cardCalendar.slot = splitValueCalendar[1];
    this.cardCalendar.letteralDaySelected = moment(splitValueCalendar[0].trim(), 'DD/MM/YYYY')
      .locale(this.calendar.locale)
      .format('LL')
      .toString();
  }

  calculateSecondsLeft(leftTime: string) {
    const now = moment(new Date());
    const end = moment(leftTime); // another date
    const duration = moment.duration(end.diff(now));
    return duration.asSeconds();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
