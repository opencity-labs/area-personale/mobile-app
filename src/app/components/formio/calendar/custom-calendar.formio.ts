import { Injector } from '@angular/core';
import { FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import { CustomCalendarComponent } from './custom-calendar.component';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'calendar',
  selector: 'custom-calendar',
  title: 'Calendar',
  group: 'basic',
  icon: 'fa fa-star',
  fieldOptions: [
    'values',
    'label',
    'key',
    'placeholder',
    'calendarId',
    'value',
    'select_opening_hours',
    'overlaps_validation',
    'allow_cancel_days',
    'opening_hours'
  ],
};

export function registerCalendarComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, CustomCalendarComponent, injector);
}
