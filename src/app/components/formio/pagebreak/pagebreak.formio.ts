import { Injector } from '@angular/core';
import { Formio, FormioCustomComponentInfo, registerCustomFormioComponent } from 'angular-formio';
import { PagebreakComponent } from './pagebreak.component';

const COMPONENT_OPTIONS: FormioCustomComponentInfo = {
  type: 'pagebreak',
  selector: 'page-break',
  title: 'PageBreak',
  group: 'basic',
  icon: 'fa fa-star',
};

export function registerPageBreakComponent(injector: Injector) {
  registerCustomFormioComponent(COMPONENT_OPTIONS, PagebreakComponent, injector);
}
