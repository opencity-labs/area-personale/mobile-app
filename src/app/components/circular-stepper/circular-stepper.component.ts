import { AfterViewInit, Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-circular-stepper',
  templateUrl: './circular-stepper.component.html',
  styleUrls: ['./circular-stepper.component.scss'],
})
export class CircularStepperComponent implements OnInit {
  @Input() steps: any[];
  @Input() activeStep = 1;
  @Input() stepTitle: string = '';

  constructor() {}

  ngOnInit() {}
}
