import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingTextCardComponent } from './loading-text-card/loading-text-card.component';
import { LoadingTextComponent } from './loading-text/loading-text.component';
import { HeaderLogosComponent } from './header-logos/header-logos.component';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [LoadingTextCardComponent, LoadingTextComponent, HeaderLogosComponent],
  imports: [CommonModule, IonicModule],
  exports: [LoadingTextCardComponent, LoadingTextComponent, HeaderLogosComponent],
})
export class SharedComponentsModule {}
