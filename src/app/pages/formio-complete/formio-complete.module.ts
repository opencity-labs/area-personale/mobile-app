import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FormioCompletePageRoutingModule } from './formio-complete-routing.module';

import { FormioCompletePage } from './formio-complete.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, FormioCompletePageRoutingModule, TranslateModule],
  declarations: [FormioCompletePage],
})
export class FormioCompletePageModule {}
