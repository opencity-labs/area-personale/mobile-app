import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationDataConfig } from '../../interfaces/applications';
import { Calendar } from '@ionic-native/calendar';
import moment from 'moment';
import { CalendarService } from '../../services/calendar.service';

@Component({
  selector: 'app-formio-complete',
  templateUrl: './formio-complete.page.html',
  styleUrls: ['./formio-complete.page.scss'],
})
export class FormioCompletePage implements OnInit {
  applicationsData: ApplicationDataConfig;
  calendar: any;
  private dateCalendar: moment.Moment;
  private hours: any;
  private startDate: any;
  private endDate: any;
  private titleCalendar: string;
  showRemainderButton: boolean;

  constructor(private route: ActivatedRoute, private router: Router, private calendarService: CalendarService) {
    this.applicationsData =
      this.router.getCurrentNavigation().extras.state &&
      this.router.getCurrentNavigation().extras.state.applicationsData
        ? this.router.getCurrentNavigation().extras.state.applicationsData
        : undefined;
    if (this.applicationsData && this.applicationsData.data.calendar) {
      this.calendar = this.applicationsData.data.calendar.split(/@|\(|#/);
      this.calendarService.getCalendar(this.calendar[2]).subscribe((res) => {
        this.titleCalendar = res.title;
        this.hours = this.calendar[1].split(/-|:/);
        this.dateCalendar = moment(this.calendar[0].trim(), 'DD/MM/YYYY');
        this.startDate = moment(this.dateCalendar).add(this.hours[0].trim(), 'h').toDate();
        this.startDate = moment(this.startDate).add(this.hours[1].trim(), 'm').toDate();
        this.endDate = moment(this.dateCalendar).add(this.hours[2].trim(), 'h').toDate();
        this.endDate = moment(this.endDate).add(this.hours[3].trim(), 'm').toDate();
        if (this.startDate && this.endDate) {
          this.showRemainderButton = true;
        }
      });
    }
  }

  ngOnInit() {}

  backHome() {
    this.router.navigate(['/tabs/home']);
  }

  openCalendar() {
    Calendar.createEventInteractively(this.titleCalendar, '', '', this.startDate, this.endDate);
  }
}
