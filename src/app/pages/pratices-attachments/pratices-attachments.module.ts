import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PraticesAttachmentsPageRoutingModule } from './pratices-attachments-routing.module';

import { PraticesAttachmentsPage } from './pratices-attachments.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, PraticesAttachmentsPageRoutingModule, TranslateModule],
  declarations: [PraticesAttachmentsPage],
})
export class PraticesAttachmentsPageModule {}
