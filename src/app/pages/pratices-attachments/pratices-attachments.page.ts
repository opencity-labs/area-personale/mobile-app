import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationsService } from '../../services/applications.service';
import { ServicesService } from '../../services/services.service';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { AuthService } from '../../auth/auth.service';
import write_blob from 'capacitor-blob-writer';
import { Directory } from '@capacitor/filesystem';
import axios from 'axios';

@Component({
  selector: 'app-pratices-attachments',
  templateUrl: './pratices-attachments.page.html',
  styleUrls: ['./pratices-attachments.page.scss'],
})
export class PraticesAttachmentsPage implements OnInit {
  service: any;
  attachments: any[];
  downloadUrl = '';
  myFiles = [];
  downloadProgress = 0;
  private token: { value: string };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private applicationsService: ApplicationsService,
    private servicesService: ServicesService,
    private fileOpener: FileOpener,
    private authenticationService: AuthService
  ) {
    this.route.queryParams.subscribe((params) => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.service = this.router.getCurrentNavigation().extras.state.detailPost;
      }
    });
  }

  ngOnInit() {}

  async downloadFile(link) {
    this.token = await this.authenticationService.getToken();
    this.downloadUrl = link ? link : this.downloadUrl;

    axios({
      url: this.downloadUrl,
      method: 'GET',
      responseType: 'blob',
      headers: { Authorization: `Bearer ${this.token.value}` },
    }).then(async (response) => {
      write_blob({
        path: new Date().getTime() + '.pdf',
        directory: Directory.Documents,
        blob: new Blob([response.data]),
        recursive: true,
        on_fallback(error) {
          console.error('err', error);
        },
      }).then((result) => {
        this.fileOpener.showOpenWithDialog(result, 'application/pdf');
      });
    });
  }
}
