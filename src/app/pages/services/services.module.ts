import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ServicesPageRoutingModule } from './services-routing.module';

import { ServicesPage } from './services.page';
import { SharedPipesModule } from '../../pipe/shared-pipes.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, ServicesPageRoutingModule, SharedPipesModule, TranslateModule],
  declarations: [ServicesPage],
})
export class ServicesPageModule {}
