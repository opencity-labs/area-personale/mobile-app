import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { ServicesService } from '../../services/services.service';
import { Subject } from 'rxjs';
import { CategoriesService } from '../../services/categories.service';
import { Categories } from '../../interfaces/categories';
import _, { groupBy } from 'lodash';
import { concatGroups } from '../../utility/grouped';

@Component({
  selector: 'app-sub-topics',
  templateUrl: './sub-topics.page.html',
  styleUrls: ['./sub-topics.page.scss'],
})
export class SubTopicsPage implements OnInit, OnDestroy {
  servicesByCategory: any[] = [];
  isLoading = true;
  private idTopics: any;
  private unsubscribe$ = new Subject<void>();
  private categories: Categories[];
  private groups: any;
  private urlFilterParams = '';
  private listServices: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private servicesService: ServicesService,
    private router: Router,
    private categoriesService: CategoriesService
  ) {
    this.activatedRoute.paramMap.subscribe((data) => {
      this.idTopics = data.get('id');
    });
    this.activatedRoute.queryParams.subscribe((params) => {
      if (
        this.router.getCurrentNavigation().extras.state &&
        this.router.getCurrentNavigation().extras.state.paramsUrl
      ) {
        this.urlFilterParams = this.router.getCurrentNavigation().extras.state.paramsUrl;
      }
    });
  }

  ngOnInit() {
    this.categoriesService
      .getCategories('true')
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        this.categories = res;
        this.servicesService
          .getGroups(this.idTopics, null, null, 'true')
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe(
            (res) => {
              this.groups = res;
              this.groups.map((g) => {
                this.servicesService
                  .getAllServices(null, g.id, true, null, null)
                  .pipe(takeUntil(this.unsubscribe$))
                  .subscribe((res) => {
                    g.value = res;
                  });
              });
              this.servicesService
                .getServicesUrlParameters(this.urlFilterParams)
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe(
                  (service) => {
                    if (this.categories) {
                      this.servicesByCategory.push({
                        category: {
                          ...this.categories.filter((cat) => cat.id === this.idTopics)[0],
                          groups: this.groups,
                          services: service,
                          subCategories: [
                            ...this.categories
                              .filter((cat) => cat.parent_id === this.idTopics)
                              .map((cat) => {
                                cat.groups = this.groups.filter((g) => g.id === cat.parent_id);
                                return cat;
                              }),
                          ],
                        },
                      });
                    }
                    this.isLoading = false;
                  },
                  (error) => {
                    this.isLoading = false;
                  },
                  () => {
                    this.isLoading = false;
                  }
                );
            },
            (error) => {}
          );
      });
  }

  navigateWithData(url: string, data) {
    const navigationExtras: NavigationExtras = {
      state: {
        data,
      },
    };
    this.router.navigate([url], navigationExtras);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getServicesUrlParams(params: string) {
    this.isLoading = true;
    this.servicesService
      .getServicesUrlParameters(params)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (res) => {
          this.servicesByCategory = [];
          this.listServices = res.filter((s) => s.status == '1' || s.status == '4');
          const groupByTopics = groupBy(this.listServices, (n) => {
            return n.topics_id;
          });
          let parentCategories = [];
          for (const [key, value] of Object.entries(groupByTopics)) {
            const getCategoryTopic = _.filter(this.categories, (cat) => cat.id === key)[0];
            const getParentCategoryTopic = _.filter(this.categories, (cat) => cat.id === getCategoryTopic.parent_id)[0];
            if (getCategoryTopic && getParentCategoryTopic) {
              parentCategories.push(getParentCategoryTopic);
            }
          }
          // Remove duplicate
          parentCategories = parentCategories.filter(
            (value, index, self) => index === self.findIndex((t) => t.slug === value.slug)
          );

          parentCategories.forEach((par_cat) => {
            const filteredGroups = _.filter(this.listServices, (ser) => ser.topics_id === par_cat.id);
            this.servicesByCategory.push({
              parent_topic: {
                name: par_cat.name,
                services: _.filter(
                  this.listServices,
                  (ser) => ser.topics_id === par_cat.id && ser.shared_with_group === false
                ),
                groups: concatGroups(filteredGroups, this.categories, this.groups),
                subCategories: [
                  ...this.categories.filter((cat) => cat.parent_id === par_cat.id),
                  /*  .map((cat) => {
                      cat.groups = this.groups.filter((g) => g.id === cat.parent_id);
                      return cat;
                    })*/
                ],
              },
            });
          });
        },
        (err) => {
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
  }

  getGroups(services: any[]) {
    services.forEach((el) => {
      el.category = _.filter(this.categories, (cat) => cat.id === el.topics_id)[0];
    });

    let filterTopics: any = _.filter(services, 'topics_id');

    filterTopics = _.groupBy(services, (item) => {
      if (item.category) return item.category.id;
    });

    const grouped = _.forEach(filterTopics, (value, key) => {
      filterTopics[key] = _.groupBy(filterTopics[key], (item) => {
        return item.service_group_id;
      });
    });

    if (!_.isEmpty(grouped)) {
      for (const [key, value] of Object.entries(grouped)) {
        Object.entries(value).forEach(([k, v]) => {
          if (k !== 'null') {
            const nameGroup = this.groups.filter((g) => g.id === k)[0];
            value[nameGroup.name] = v;
            value[k].show = this.searchGroups(v);
            delete value[k];
            return value;
          } else {
            value[k].show = true;
            return value[k];
          }
        });
        return value;
      }
    } else {
      return [];
    }
  }
  searchGroups(v: any) {
    return v.some((el) => el.shared_with_group && el.shared_with_group === false);
  }
}
