/*
import { Injector, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FormPageRoutingModule } from './form-routing.module';
import { FormPage } from './form.page';
import { FormioModule } from 'angular-formio';
import { registerPageBreakComponent } from 'src/app/components/formio/pagebreak/pagebreak.formio';
import { CircularStepperComponent } from '../../components/circular-stepper/circular-stepper.component';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { registerCalendarComponent } from '../../components/formio/calendar/custom-calendar.formio';
import { NgCalendarModule } from 'ionic2-calendar';
import { CustomCalendarComponent } from '../../components/formio/calendar/custom-calendar.component';
import { registerRatingComponent } from '../../rating-wrapper/rating-wrapper.formio';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RatingWrapperComponent } from '../../rating-wrapper/rating-wrapper.component';
import { CountdownModule } from 'ngx-countdown';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FormPageRoutingModule,
    FormioModule,
    RoundProgressModule,
    BsDropdownModule,
    NgCalendarModule,
    NgbModule,
    CountdownModule,
  ],
  providers: [],
  declarations: [FormPage, CircularStepperComponent, RatingWrapperComponent, CustomCalendarComponent],
  entryComponents: [],
})
export class FormPageModule {
  constructor(injector: Injector) {
    registerRatingComponent(injector);
    registerPageBreakComponent(injector);
    registerCalendarComponent(injector);
  }
}
*/
