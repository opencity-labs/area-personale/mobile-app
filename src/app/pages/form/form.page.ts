import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { FormioOptions } from 'angular-formio';
import { AlertController, LoadingController, ModalController } from '@ionic/angular';
import { OptionsFormio } from '../../translation/formio';
import { FormioBaseComponent } from '@formio/angular';
import { DescriptionServicePage } from '../../modals/description-service/description-service.page';
import { ApplicationsService } from '../../services/applications.service';
import { takeUntil } from 'rxjs/operators';
import { StorageService } from '../../services/storage.service';
import { UserService } from '../../services/user.service';
import { forkJoin, Subject } from 'rxjs';
import { User } from '../../interfaces/user';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.page.html',
  styleUrls: ['./form.page.scss'],
})
export class FormPage implements OnInit, OnDestroy {
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public loadingController: LoadingController,
    public modalController: ModalController,
    private applicationsService: ApplicationsService,
    private storageService: StorageService,
    private userService: UserService,
    public alertController: AlertController,
    private translateService: TranslateService
  ) {
    this.route.queryParams.subscribe((params) => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.service =
          this.router.getCurrentNavigation().extras.state.detailPost ||
          this.router.getCurrentNavigation().extras.state.data;
        this.urlForm = this.service.flow_steps[0].parameters.url + this.service.flow_steps[0].parameters.formio_id;

        this.storageService.decodeUser().then((user) => {
          if (user) {
            this.userService
              .getUser(user.id)
              .pipe(takeUntil(this.unsubscribe$))
              .subscribe(async (res) => {
                this.user = res;
                this.data = {
                  data: {
                    applicant: {
                      data: {
                        email_address: this.user.email,
                        completename: {
                          data: {
                            name: this.user.nome,
                            surname: this.user.cognome,
                          },
                        },
                        /*    gender: {
                          data: {
                            gender: this.user.sesso === 'M' ? 'maschio' : 'femmina',
                          }
                        },*/
                        fiscal_code: {
                          data: {
                            fiscal_code: this.user.codice_fiscale,
                          },
                        },
                        /*   Born: {
                          data: {
                            natoAIl: moment(this.user.data_nascita).format('DD/MM/YYYY'),
                            place_of_birth: this.user.luogo_nascita,
                          }
                        },
                        address: {
                          data: {
                            address: this.user.indirizzo_residenza,
                            house_number: '',
                            municipality: this.user.citta_residenza,
                            postal_code: this.user.cap_residenza,
                            county: this.user.provincia_residenza,
                          },
                        },*/
                      },
                    },
                  },
                };
              });
          }
        });

        forkJoin([
          this.applicationsService.getFormIOService(this.urlForm),
          this.applicationsService.getFormIOService(this.urlForm + '/i18n'),
        ])
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe(
            async (results) => {
              this.applicantForm = results[0];
              this.options.i18n = results[1];
              if (this.formioReference)
                this.formioReference.formio.language = this.translateService.getDefaultLang() || 'it';
              await this.presentLoading();
            },
            async (err) => {
              await this.presentLoading();
            }
          );
      }
    });
  }

  @ViewChild('formio', { read: ElementRef }) formio: FormioBaseComponent;

  renderOptions = {};
  rendered = false;
  service: any;
  applicantForm: any;
  options: FormioOptions = OptionsFormio;
  wizard: any;
  showSubmit: boolean;
  showPrevious: boolean;
  showNext: boolean;
  showCancel: boolean;
  urlForm: string;
  steps: any[] = [];
  activeStep = 1;
  stepTitle: any;
  private formioReference: FormioBaseComponent;
  private loading: HTMLIonLoadingElement;
  formIsReady = false;
  data: any = { data: {} };
  private unsubscribe$ = new Subject<void>();
  user: User;

  ngOnInit() {}

  onNextPage() {
    const nextButton = this.formioReference.formioElement.nativeElement.getElementsByClassName('btn-wizard-nav-next');
    nextButton[0].click();
    this.checkViewedButtons();
  }

  onNextPageEvent($event) {
    this.activeStep++;
  }

  onSummary() {
    this.checkViewedButtons();
    this.formioReference.formio.emit('errorChange');
    if (!this.formioReference.formio.checkValidity(null, 'dirty', null, false)) {
      this.formioReference.formio.checkValidity(null, 'dirty', null, false);
    } else {
      this.service.data = this.formioReference.submission.data;
      const navigationExtras: NavigationExtras = {
        state: {
          detailPost: this.service,
        },
      };
      this.router.navigate(['/formio-summary'], navigationExtras);
    }
  }

  checkViewedButtons() {
    setTimeout(() => {
      this.showSubmit =
        this.formioReference.formioElement.nativeElement.getElementsByClassName('btn-wizard-nav-submit').length > 0;
      this.showPrevious =
        this.formioReference.formioElement.nativeElement.getElementsByClassName('btn-wizard-nav-previous').length > 0;
      this.showNext =
        this.formioReference.formioElement.nativeElement.getElementsByClassName('btn-wizard-nav-next').length > 0;
      this.showCancel =
        this.formioReference.formioElement.nativeElement.getElementsByClassName('btn-wizard-nav-cancel').length > 0;
      this.steps = this.formioReference.formioElement.nativeElement.getElementsByClassName('page-item');
      this.stepTitle = this.getStepTitle(
        this.formioReference.formioElement.nativeElement.querySelectorAll('.page-item.active > .page-link')
      );
    }, 200);
  }

  onPreviousPage() {
    const prevButton =
      this.formioReference.formioElement.nativeElement.getElementsByClassName('btn-wizard-nav-previous');
    prevButton[0].click();
    this.checkViewedButtons();
  }
  onPreviousPageEvent($event) {
    this.activeStep--;
  }

  async onCancel() {
    // Native reset button form.io
    // const prevButton = this.formioReference.formioElement.nativeElement.getElementsByClassName('btn-wizard-nav-cancel');
    const alert = await this.alertController.create({
      cssClass: '',
      header: this.translateService.instant('formio.annullare_modulo'),
      message: '',
      buttons: [
        {
          text: this.translateService.instant('common.chiudi'),
          role: 'cancel',
          cssClass: 'secondary',
          id: 'cancel-button',
          handler: () => {},
        },
        {
          text: this.translateService.instant('common.conferma'),
          id: 'confirm-button',
          handler: () => {
            this.formioReference.formio.emit('resetForm');
          },
        },
      ],
    });

    await alert.present();
    // Click native reset button form.io
    // prevButton[0].click();
    this.checkViewedButtons();
  }

  // Get title of current step
  getStepTitle(el: any) {
    if (el && el.length > 0) {
      return el[0].innerText;
    }
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      cssClass: '',
      message: this.translateService.instant('common.recupero_dati'),
    });
    await this.loading.present();
  }
  onReady($event) {
    this.formioReference = $event;
    this.formioReference.formio.language = this.translateService.getDefaultLang() || 'it';
    this.checkViewedButtons();
  }

  onLoad($event) {
    this.loading.dismiss().then(() => {
      this.formIsReady = true;
    });
  }

  async openDescriptionModal() {
    const modal = await this.modalController.create({
      component: DescriptionServicePage,
      cssClass: '',
      componentProps: {
        service: this.service,
      },
    });
    return await modal.present();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
