import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'profile',
        children: [
          {
            path: '',
            loadChildren: () => import('../../pages/profile/profile.module').then((m) => m.ProfilePageModule),
            //  canActivate: [AuthGuardGuard],
          },
        ],
      },
      {
        path: 'services',
        children: [
          {
            path: '',
            loadChildren: () => import('../../pages/services/services.module').then((m) => m.ServicesPageModule),
          },
        ],
      },
      {
        path: 'home',
        children: [
          {
            path: '',
            loadChildren: () => import('../../pages/home/home.module').then((m) => m.HomePageModule),
          },
        ],
      },
      {
        path: 'practices',
        children: [
          {
            path: '',
            loadChildren: () => import('../../pages/practices/practices.module').then((m) => m.PracticesPageModule),
          },
        ],
      },
      {
        path: 'topics',
        children: [
          {
            path: '',
            loadChildren: () => import('../../pages/topics/topics.module').then((m) => m.TopicsPageModule),
          },
        ],
      },
      {
        path: '',
        redirectTo: '/tabs/home',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
