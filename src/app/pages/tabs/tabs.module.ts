import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TabsPageRoutingModule } from './tabs-routing.module';

import { TabsPage } from './tabs.page';
import { StorageService } from '../../services/storage.service';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [CommonModule, FormsModule, TabsPageRoutingModule, IonicModule, TranslateModule],
  providers: [StorageService],
  declarations: [TabsPage],
})
export class TabsPageModule {}
