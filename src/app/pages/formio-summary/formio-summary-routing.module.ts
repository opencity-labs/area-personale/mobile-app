import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormioSummaryPage } from './formio-summary.page';

const routes: Routes = [
  {
    path: '',
    component: FormioSummaryPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FormioSummaryPageRoutingModule {}
