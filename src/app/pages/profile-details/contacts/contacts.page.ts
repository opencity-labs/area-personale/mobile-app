import { Component, OnDestroy, OnInit } from '@angular/core';
import { User } from '../../../interfaces/user';
import { UserService } from '../../../services/user.service';
import { StorageService } from '../../../services/storage.service';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.page.html',
  styleUrls: ['./contacts.page.scss'],
})
export class ContactsPage implements OnInit, OnDestroy {
  private loading: HTMLIonLoadingElement;
  user: User;
  enableEdit = false;
  profileForm: UntypedFormGroup;
  private unsubscribe$ = new Subject<void>();

  constructor(
    private userService: UserService,
    private storageService: StorageService,
    public alertController: AlertController,
    public loadingController: LoadingController,
    public formBuilder: UntypedFormBuilder,
    public toastController: ToastController,
    public router: Router,
    private translateService: TranslateService
  ) {
    this.profileForm = this.formBuilder.group({
      cellulare: [
        {
          value: null,
        },
      ],
      telefono: [
        {
          value: null,
        },
      ],
    });
  }

  ngOnInit() {
    this.presentLoading().then(() => {
      this.storageService
        .decodeUser()
        .then((user) => {
          this.userService
            .getUser(user.id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
              async (res) => {
                this.user = res;
                this.profileForm.patchValue({
                  telefono: this.user.telefono,
                  cellulare: this.user.cellulare,
                });
                this.loading.dismiss();
              },
              (err) => {
                this.loading.dismiss();
              }
            );
        })
        .catch((err) => {
          this.loading.dismiss();
          this.router.navigate(['error-page']);
        });
    });
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      cssClass: '',
      message: this.translateService.instant('common.recupero_dati'),
    });
    await this.loading.present();
  }

  edit() {
    this.enableEdit = !this.enableEdit;
  }

  onSubmit() {
    const idUser = this.user.id;
    this.user = {
      ...this.user,
      telefono: this.profileForm.get('telefono').value,
      cellulare: this.profileForm.get('cellulare').value,
    };

    delete this.user.id;
    this.profileForm.patchValue(this.user);

    this.userService.patchUser(idUser, this.user).subscribe(
      (res) => {
        this.presentToast();
      },
      (error) => {
        console.error(error);
        this.presentAlertError(error);
      }
    );
  }

  get errorControl() {
    return this.profileForm.controls;
  }

  submitForm() {}

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  async presentAlertError(error: any) {
    const alert = await this.alertController.create({
      cssClass: '',
      header: error.error.title || 'Ops',
      message: error.error.description || 'Si sono verificati errori durante il processo di invio',
      buttons: [
        {
          text: this.translateService.instant('common.chiudi'),
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {},
        },
      ],
    });

    await alert.present();
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: this.translateService.instant('messaggi.dati_aggiornati'),
      duration: 2000,
    });
    toast.present();
  }

  async presentAlertSuccess() {
    const alert = await this.alertController.create({
      cssClass: '',
      header: '',
      message: this.translateService.instant('messaggi.dati_aggiornati'),
      buttons: [
        {
          text: this.translateService.instant('common.chiudi'),
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {},
        },
      ],
    });

    await alert.present();
  }
}
