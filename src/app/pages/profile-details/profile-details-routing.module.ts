import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileDetailsPage } from './profile-details.page';

const routes: Routes = [
  {
    path: '',
    component: ProfileDetailsPage,
  },
  {
    path: 'user',
    loadChildren: () => import('./user/user.module').then((m) => m.UserPageModule),
  },
  {
    path: 'contacts',
    loadChildren: () => import('./contacts/contacts.module').then((m) => m.ContactsPageModule),
  },
  {
    path: 'residence',
    loadChildren: () => import('./residence/residence.module').then((m) => m.ResidencePageModule),
  },
  {
    path: 'domicile',
    loadChildren: () => import('./domicile/domicile.module').then((m) => m.DomicilePageModule),
  },
  {
    path: 'document',
    loadChildren: () => import('./document/document.module').then((m) => m.DocumentPageModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileDetailsPageRoutingModule {}
