import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { StorageService } from '../../../services/storage.service';
import { AlertController, LoadingController } from '@ionic/angular';
import { User } from '../../../interfaces/user';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.page.html',
  styleUrls: ['./user.page.scss'],
})
export class UserPage implements OnInit {
  user: User;
  private loading: HTMLIonLoadingElement;
  constructor(
    private userService: UserService,
    private storageService: StorageService,
    public alertController: AlertController,
    public loadingController: LoadingController,
    public router: Router,
    private translateService: TranslateService
  ) {}

  ngOnInit() {
    this.presentLoading().then(() => {
      this.storageService
        .decodeUser()
        .then((user) => {
          this.userService.getUser(user.id).subscribe(
            async (res) => {
              this.user = res;
              this.loading.dismiss();
            },
            (err) => {
              this.loading.dismiss();
            }
          );
        })
        .catch((err) => {
          this.loading.dismiss();
          this.router.navigate(['error-page']);
        });
    });
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      cssClass: '',
      message: this.translateService.instant('common.recupero_dati'),
    });
    await this.loading.present();
  }
}
