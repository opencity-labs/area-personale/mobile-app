import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { StorageService } from '../../../services/storage.service';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { User } from '../../../interfaces/user';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-residence',
  templateUrl: './residence.page.html',
  styleUrls: ['./residence.page.scss'],
})
export class ResidencePage implements OnInit {
  private loading: HTMLIonLoadingElement;
  user: User;
  enableEdit = false;
  profileForm: UntypedFormGroup;
  private unsubscribe$ = new Subject<void>();

  constructor(
    private userService: UserService,
    private storageService: StorageService,
    public alertController: AlertController,
    public loadingController: LoadingController,
    public formBuilder: UntypedFormBuilder,
    public toastController: ToastController,
    public router: Router,
    private translateService: TranslateService
  ) {
    this.profileForm = this.formBuilder.group({
      indirizzo_residenza: [
        {
          value: null,
        },
      ],
      cap_residenza: [
        {
          value: null,
        },
      ],
      citta_residenza: [
        {
          value: null,
        },
      ],
      provincia_residenza: [
        {
          value: null,
        },
      ],
      stato_residenza: [
        {
          value: null,
        },
      ],
    });
  }

  ngOnInit() {
    this.presentLoading().then(() => {
      this.storageService
        .decodeUser()
        .then((user) => {
          this.userService
            .getUser(user.id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
              async (res) => {
                this.user = res;
                this.profileForm.patchValue({
                  indirizzo_residenza: this.user.indirizzo_residenza,
                  cap_residenza: this.user.cap_residenza,
                  citta_residenza: this.user.citta_residenza,
                  provincia_residenza: this.user.provincia_residenza,
                  stato_residenza: this.user.stato_residenza,
                });
                this.loading.dismiss();
              },
              (err) => {
                this.loading.dismiss();
              }
            );
        })
        .catch((err) => {
          this.loading.dismiss();
          this.router.navigate(['error-page']);
        });
    });
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      cssClass: '',
      message: this.translateService.instant('messaggi.dati_aggiornati'),
    });
    await this.loading.present();
  }

  edit() {
    this.enableEdit = !this.enableEdit;
  }

  onSubmit() {
    this.user = {
      ...this.user,
      indirizzo_residenza: this.profileForm.get('indirizzo_residenza').value,
      cap_residenza: this.profileForm.get('cap_residenza').value,
      citta_residenza: this.profileForm.get('citta_residenza').value,
      provincia_residenza: this.profileForm.get('provincia_residenza').value,
      stato_residenza: this.profileForm.get('stato_residenza').value,
    };
    this.profileForm.patchValue(this.user);
    this.userService.patchUser(this.user.id, this.user).subscribe(
      (res) => {
        this.presentToast();
      },
      (error) => {
        this.presentAlertError(error);
      }
    );
  }

  get errorControl() {
    return this.profileForm.controls;
  }

  submitForm() {}

  async presentAlertError(error: any) {
    const alert = await this.alertController.create({
      cssClass: '',
      header: error.error.title || 'Ops',
      message: error.error.description || 'Si sono verificati errori durante il processo di invio',
      buttons: [
        {
          text: this.translateService.instant('common.chiudi'),
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {},
        },
      ],
    });

    await alert.present();
  }

  async presentAlertSuccess() {
    const alert = await this.alertController.create({
      cssClass: '',
      header: '',
      message: this.translateService.instant('messaggi.dati_aggiornati'),
      buttons: [
        {
          text: 'Chiudi',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {},
        },
      ],
    });

    await alert.present();
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: this.translateService.instant('messaggi.dati_aggiornati'),
      duration: 2000,
    });
    toast.present();
  }
}
