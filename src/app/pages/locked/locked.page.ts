import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { FingerprintAIO, FingerprintOptions } from '@ionic-native/fingerprint-aio/ngx';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-locked',
  templateUrl: './locked.page.html',
  styleUrls: ['./locked.page.scss'],
})
export class LockedPage {
  constructor(
    private modalCtrl: ModalController,
    public faio: FingerprintAIO,
    public toastCtrl: ToastController,
    private translateService: TranslateService
  ) {}

  dismissLockScreen() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      dismissed: true,
    });
  }

  showFingerPrint($event) {
    const options: FingerprintOptions = {
      title: this.translateService.instant('sicurezza.autenticazione_biometrica'),
      subtitle: '',
      description: this.translateService.instant('sicurezza.autenticazione_biometrica_descrizione'),
      fallbackButtonTitle: this.translateService.instant('sicurezza.usa_codice'),
      cancelButtonTitle: this.translateService.instant('common.chiudi'),
      disableBackup: false,
    };
    this.faio.isAvailable().then(
      (isAvailable) => {
        if (isAvailable === 'finger' || isAvailable === 'face' || isAvailable === 'biometric') {
          this.faio
            .show(options)
            .then((result: any) => {
              this.dismissLockScreen();
            })
            .catch((e) => {
              console.log('Inside failure on cancel');
            });
        }
      },
      (error) => {
        alert(error.message);
        console.log('isAvailable error', error);
      }
    );
  }
}
