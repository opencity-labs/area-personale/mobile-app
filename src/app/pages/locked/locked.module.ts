import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LockedPageRoutingModule } from './locked-routing.module';

import { LockedPage } from './locked.page';
import { TranslateModule } from '@ngx-translate/core';
import { SharedComponentsModule } from '../../components/shared-components.module';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, LockedPageRoutingModule, TranslateModule, SharedComponentsModule],
  declarations: [LockedPage],
})
export class LockedPageModule {}
