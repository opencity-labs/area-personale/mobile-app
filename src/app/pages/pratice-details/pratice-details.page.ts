import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { Calendar } from '@ionic-native/calendar';
import { ApplicationsService } from '../../services/applications.service';
import { Subject } from 'rxjs';
import moment from 'moment';
import { CalendarService } from '../../services/calendar.service';
import { takeUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-pratice-details',
  templateUrl: './pratice-details.page.html',
  styleUrls: ['./pratice-details.page.scss'],
})
export class PraticeDetailsPage implements OnInit, OnDestroy {
  applicationDetails: any;
  cardCalendar: any = {};
  calendar: any;
  showRemainderButton: boolean;
  isPassedDate: boolean;
  private unsubscribe$ = new Subject<void>();
  private dateCalendar: moment.Moment;
  private hours: any;
  private startDate: any;
  private endDate: any;
  private titleCalendar: string;

  constructor(
    private route: ActivatedRoute,
    private applicationsService: ApplicationsService,
    private router: Router,
    private calendarService: CalendarService,
    private translateService: TranslateService,
    public alertController: AlertController
  ) {
    this.route.queryParams.subscribe((params) => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.applicationDetails =
          this.router.getCurrentNavigation().extras.state.detailPost ||
          this.router.getCurrentNavigation().extras.state.data.group;
        if (this.applicationDetails && this.applicationDetails.data.calendar) {
          this.getDateCalendar(this.applicationDetails.data.calendar);
          this.calendar = this.applicationDetails.data.calendar.split(/@|\(|#/);
          this.calendarService
            .getCalendar(this.calendar[2])
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((res) => {
              this.titleCalendar = res.title;
              this.hours = this.calendar[1].split(/-|:/);
              this.dateCalendar = moment(this.calendar[0].trim(), 'DD/MM/YYYY');
              this.startDate = moment(this.dateCalendar).add(this.hours[0].trim(), 'h').toDate();
              this.startDate = moment(this.startDate).add(this.hours[1].trim(), 'm').toDate();
              this.endDate = moment(this.dateCalendar).add(this.hours[2].trim(), 'h').toDate();
              this.endDate = moment(this.endDate).add(this.hours[3].trim(), 'm').toDate();
              if (this.startDate && this.endDate) {
                this.showRemainderButton = true;
                this.isPassedDate =
                  moment().isSameOrAfter(this.startDate, 'days') || !(parseInt(this.applicationDetails.status) < 8000);
              }
            });
        }
      }
    });
  }

  ngOnInit() {}

  openDetails(url: string, data) {
    const navigationExtras: NavigationExtras = {
      state: {
        detailPost: data,
      },
    };
    this.router.navigate([url], navigationExtras);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getDateCalendar(date: string) {
    const splitValueCalendar = date.split(/@|\(/);
    this.cardCalendar.slot = splitValueCalendar[1];
    this.cardCalendar.letteralDaySelected = moment(splitValueCalendar[0].trim(), 'DD/MM/YYYY')
      .locale(this.translateService.getDefaultLang() || 'it')
      .format('DD MMM YY')
      .toString();
  }

  openCalendar(service: any) {
    Calendar.createEventInteractively(
      service.service_name + ' - ' + this.titleCalendar,
      '',
      '',
      this.startDate,
      this.endDate
    );
  }

  async presentAlertDeleteCalendar() {
    const alert = await this.alertController.create({
      cssClass: '',
      header: 'Stai per cancellare un appuntamento',
      message:
        "Sei sicuro di voler cancellare l'appuntamento?<br> L'azione è <strong>irreversibile</strong><br> Conferma per procedere.",
      buttons: [
        {
          text: 'Chiudi',
          role: 'cancel',
          cssClass: 'secondary',
          id: 'cancel-button',
          handler: () => {
            console.log('Confirm Cancel: blah');
          },
        },
        {
          text: 'Conferma',
          id: 'confirm-button',
          handler: () => {
            console.log('Confirm Okay');
          },
        },
      ],
    });

    await alert.present();
  }
}
