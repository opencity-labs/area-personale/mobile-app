import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PraticeDetailsPageRoutingModule } from './pratice-details-routing.module';

import { PraticeDetailsPage } from './pratice-details.page';
import { SharedPipesModule } from '../../pipe/shared-pipes.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PraticeDetailsPageRoutingModule,
    SharedPipesModule,
    TranslateModule,
  ],
  declarations: [PraticeDetailsPage],
  exports: [],
})
export class PraticeDetailsPageModule {}
