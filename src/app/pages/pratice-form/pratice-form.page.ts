import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationDataConfig } from '../../interfaces/applications';
import { ApplicationsService } from '../../services/applications.service';
import { unflattenObject } from '../../utility/unflatten';
import { ServicesService } from '../../services/services.service';
import { forkJoin, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { FormioBaseComponent } from '@formio/angular';
import { FormioOptions } from 'angular-formio';
import { OptionsFormio } from '../../translation/formio';

@Component({
  selector: 'app-pratice-form',
  templateUrl: './pratice-form.page.html',
  styleUrls: ['./pratice-form.page.scss'],
})
export class PraticeFormPage implements OnInit, OnDestroy {
  application: ApplicationDataConfig;
  formId: string;
  src: string;
  form: any;
  service: any;
  data: any = {};
  urlForm: string;
  applicantForm: any;
  isLoading = true;
  errorMessage: string;
  private unsubscribe$ = new Subject<void>();
  private formioReference: FormioBaseComponent;
  options: FormioOptions = OptionsFormio;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private applicationsService: ApplicationsService,
    private servicesService: ServicesService,
    private translateService: TranslateService
  ) {
    this.route.queryParams.subscribe((params) => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.service = this.router.getCurrentNavigation().extras.state.detailPost;
        const flatten = unflattenObject(this.service.data);
        this.data = {
          data: {
            ...flatten,
          },
        };
        this.servicesService
          .getServiceById(this.service.service_id)
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe((res) => {
            const tmp = res;
            this.urlForm = tmp.flow_steps[0].parameters.url + tmp.flow_steps[0].parameters.formio_id;
            forkJoin([
              this.applicationsService.getFormIOService(this.urlForm),
              this.applicationsService.getFormIOService(this.urlForm + '/i18n'),
            ])
              .pipe(takeUntil(this.unsubscribe$))
              .subscribe(
                (results) => {
                  results[0].display = 'form';
                  this.applicantForm = results[0];
                  this.isLoading = false;
                  this.options.i18n = results[1];
                },
                (error) => {
                  if (error.status === 404) {
                    this.errorMessage = this.translateService.instant('error.modulo_non_trovato');
                  }
                },
                () => {
                  //this.formioReference.formio.language = this.translateService.getDefaultLang() || 'it'
                  this.isLoading = false;
                }
              );
          });
      }
    });
  }

  ngOnInit() {}

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  onReady($event) {
    this.formioReference = $event;
    this.formioReference.formio.language = this.translateService.getDefaultLang() || 'it';
  }
}
