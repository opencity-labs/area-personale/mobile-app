import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PraticeFormPage } from './pratice-form.page';

const routes: Routes = [
  {
    path: '',
    component: PraticeFormPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PraticeFormPageRoutingModule {}
