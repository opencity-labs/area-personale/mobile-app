import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/auth.service';
import { Browser } from '@capacitor/browser';
import { environment } from '../../../environments/environment';
import { InAppBrowser, InAppBrowserObject } from '@ionic-native/in-app-browser/ngx';
import { StorageService } from '../../services/storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  private browserRef: InAppBrowserObject;
  constructor(
    private authService: AuthService,
    private router: Router,
    private iab: InAppBrowser,
    private storage: StorageService
  ) {}

  ngOnInit() {
    this.onClickSpidButton();
  }

  ionViewWillEnter() {
    this.onClickSpidButton();
  }

  onClickSpidButton() {
    this.browserRef = this.iab.create(
      environment.api_stanza + '/auth/login-pat?format=jwt',
      '_blank',
      'hidden=no,location=yes,clearsessioncache=yes,clearcache=yes,hidenavigationbuttons=yes,hideurlbar=yes,footer=no,toolbar=yes'
    );

    this.browserRef.on('loadstart').subscribe((e) => {
      this.loadStartCallBack(e);
    });

    /* this.browserRef.on('loadstop').subscribe(() => {
    });

    this.browserRef.on('loaderror').subscribe(() => {
    });

    this.browserRef.on('beforeload').subscribe(() => {
    });

    this.browserRef.on('exit').subscribe((res) => {
    });*/
  }

  async loadStartCallBack(event) {
    // Close InAppBrowser if loading the predefined close URL
    if (event.url.includes('login-success') && event.url.includes('token')) {
      const urlParams = new URL(decodeURIComponent(event.url));
      const params = (urlParams.toString()).split('token=');
      const token = params[1];

      await this.storage
        .set('ACCESS_TOKEN', token)
        .then((r) => {
          setTimeout(() => {
            this.authService.isAuthenticated.next(true);
            this.router.navigateByUrl('/tabs/home');
            this.browserRef.close();
          }, 500);
        })
        .catch((err) => {
          console.log('err', err);
        });
    }
  }

  openPage() {
    Browser.open({ url: 'https://www.spid.gov.it/cos-e-spid/come-attivare-spid/' });
  }
}
