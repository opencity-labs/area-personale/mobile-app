import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { StorageService } from '../../services/storage.service';
import { Subject } from 'rxjs';
import { ApplicationsService } from 'src/app/services/applications.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { ApplicationsConfig } from '../../interfaces/applications';
import { takeUntil } from 'rxjs/operators';
import { ServicesService } from '../../services/services.service';
import { HelpPagePage } from '../../modals/help-page/help-page.page';
import { LoadingController, ModalController } from '@ionic/angular';
import { getDateCalendar } from '../../utility/calendar';
import { TranslateService } from '@ngx-translate/core';
import { CalendarMode, Step } from 'ionic2-calendar';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit, OnDestroy {
  private loading: HTMLIonLoadingElement;
  private unsubscribe$ = new Subject<void>();
  public selectedSection = 'home';
  token: string;
  municipalityData: any;
  counter: number;
  services: any[] = [];
  groups: any[];
  calendar = {
    mode: 'week' as CalendarMode,
    step: 30 as Step,
    locale: 'it-IT',
    currentDate: undefined,
  };
  applications: ApplicationsConfig = {
    links: null,
    meta: null,
    data: [],
  };
  showSpinner: boolean;

  constructor(
    private iab: InAppBrowser,
    private router: Router,
    private storageService: StorageService,
    private applicationsService: ApplicationsService,
    private servicesService: ServicesService,
    public modalController: ModalController,
    public loadingController: LoadingController,
    private translateService: TranslateService
  ) {}

  ngOnInit() {
    if (!this.applications.data.length) {
      this.applicationsService
        .getApplications('creationTime', '5', 'desc')
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe((res) => {
          this.applications.data = res.data;
          if (this.applications.data.length > 0) {
            this.applications.data.map((item) => {
              if (item.data.calendar) {
                item.parse_data_calendar = getDateCalendar(
                  item.data.calendar,
                  this.translateService.getDefaultLang() || 'it'
                );
              }
            });
          }
        });
    }
  }

  updateApplications(event?: any) {
    this.showSpinner = true;
    this.applicationsService
      .getApplications('creationTime', '5', 'desc')
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (res) => {
          this.applications.data = res.data;
          if (this.applications.data.length > 0) {
            this.applications.data.map((item) => {
              if (item.data.calendar) {
                item.parse_data_calendar = getDateCalendar(
                  item.data.calendar,
                  this.translateService.getDefaultLang() || 'it'
                );
              }
            });
          }
          this.showSpinner = false;
          if (event) {
            event.target.complete();
          }
        },
        (err) => {
          this.showSpinner = false;
        }
      );
  }

  ionViewWillEnter() {
    this.presentLoading().then(() => {
      this.showSpinner = true;
      this.applicationsService
        .getApplications('creationTime', '5', 'desc')
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          (res) => {
            this.applications.data = res.data;
            if (this.applications.data.length > 0) {
              this.applications.data.map((item) => {
                if (item.data.calendar) {
                  item.parse_data_calendar = getDateCalendar(
                    item.data.calendar,
                    this.translateService.getDefaultLang() || 'it'
                  );
                }
              });
            }
            this.showSpinner = false;
            this.loading.dismiss();
          },
          (err) => {
            this.showSpinner = false;
            this.loading.dismiss();
          }
        );
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  openDetails(url: string, group: any) {
    const navigationExtras: NavigationExtras = {
      state: {
        data: {
          group,
        },
      },
    };
    this.router.navigate([url], navigationExtras);
  }

  async openHelpModal() {
    const modal = await this.modalController.create({
      component: HelpPagePage,
      cssClass: '',
      componentProps: {
        service: null,
      },
    });
    return await modal.present();
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      cssClass: '',
      message: this.translateService.instant('common.recupero_dati'),
    });
    await this.loading.present();
  }

  doRefresh(event: any) {
    this.updateApplications(event);
  }
}
