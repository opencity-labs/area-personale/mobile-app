import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GroupDetailsPageRoutingModule } from './group-details-routing.module';

import { GroupDetailsPage } from './group-details.page';
import { ServiceDetailsPageModule } from '../service-details/service-details.module';
import { SharedPipesModule } from '../../pipe/shared-pipes.module';
import { TranslateModule } from '@ngx-translate/core';
import { SharedDirectivesModule } from '../../directives/shared-directives.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GroupDetailsPageRoutingModule,
    ServiceDetailsPageModule,
    SharedPipesModule,
    TranslateModule,
    SharedDirectivesModule,
  ],
  declarations: [GroupDetailsPage],
})
export class GroupDetailsPageModule {}
