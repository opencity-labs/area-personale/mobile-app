import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ServicesService } from '../../services/services.service';

@Component({
  selector: 'app-group-details',
  templateUrl: './group-details.page.html',
  styleUrls: ['./group-details.page.scss'],
})
export class GroupDetailsPage implements OnInit {
  group: any;
  servicesGroup: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private servicesService: ServicesService,
    private router: Router
  ) {
    this.activatedRoute.queryParams.subscribe((params) => {
      if (this.router.getCurrentNavigation().extras.state && this.router.getCurrentNavigation().extras.state.data) {
        this.servicesGroup = this.router.getCurrentNavigation().extras.state.data;
      }
    });
  }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((data) => {
      this.servicesService.getGroupsById(data.get('id')).subscribe((res) => {
        this.group = res;
      });
    });
  }

  navigateWithData(url: string, data) {
    const navigationExtras: NavigationExtras = {
      state: {
        data,
      },
    };
    this.router.navigate([url], navigationExtras);
  }

  protected readonly JSON = JSON;
}
