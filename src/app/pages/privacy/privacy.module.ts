import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PrivacyPageRoutingModule } from './privacy-routing.module';

import { PrivacyPage } from './privacy.page';
import { TranslateModule } from '@ngx-translate/core';
import { SharedPipesModule } from '../../pipe/shared-pipes.module';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, PrivacyPageRoutingModule, TranslateModule, SharedPipesModule],
  declarations: [PrivacyPage],
})
export class PrivacyPageModule {}
