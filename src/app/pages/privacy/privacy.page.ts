import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { HelpPagePage } from '../../modals/help-page/help-page.page';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.page.html',
  styleUrls: ['./privacy.page.scss'],
})
export class PrivacyPage implements OnInit {
  helpUrl: any;
  constructor(public sanitizer: DomSanitizer, public modalController: ModalController) {}

  ngOnInit() {
    this.helpUrl = this.sanitizer.bypassSecurityTrustResourceUrl('');
  }

  async openHelpModal() {
    const modal = await this.modalController.create({
      component: HelpPagePage,
      cssClass: '',
      componentProps: {
        service: null,
      },
    });
    return await modal.present();
  }
}
