import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PraticesMessagesPageRoutingModule } from './pratices-messages-routing.module';

import { PraticesMessagesPage } from './pratices-messages.page';
import { SharedPipesModule } from '../../pipe/shared-pipes.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PraticesMessagesPageRoutingModule,
    SharedPipesModule,
    TranslateModule,
  ],
  declarations: [PraticesMessagesPage],
})
export class PraticesMessagesPageModule {}
