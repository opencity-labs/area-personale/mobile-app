import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ServiceDetailsPageRoutingModule } from './service-details-routing.module';
import { ServiceDetailsPage } from './service-details.page';
import { SharedPipesModule } from '../../pipe/shared-pipes.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ServiceDetailsPageRoutingModule,
    ReactiveFormsModule,
    SharedPipesModule,
    TranslateModule,
  ],
  exports: [],
  declarations: [ServiceDetailsPage],
})
export class ServiceDetailsPageModule {}
