import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { UntypedFormBuilder } from '@angular/forms';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { AlertController, LoadingController, ModalController } from '@ionic/angular';
import { HelpPagePage } from '../../modals/help-page/help-page.page';

@Component({
  selector: 'app-service-details',
  templateUrl: './service-details.page.html',
  styleUrls: ['./service-details.page.scss'],
})
export class ServiceDetailsPage implements OnInit, OnDestroy {
  services: any[] = [];
  private unsubscribe$ = new Subject<void>();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public formBuilder: UntypedFormBuilder,
    public loadingController: LoadingController,
    public alertController: AlertController,
    public modalController: ModalController
  ) {
    this.route.queryParams.subscribe((params) => {
      if (this.router.getCurrentNavigation().extras.state) {
        if (this.router.getCurrentNavigation().extras.state.detailPost) {
          this.services.push(this.router.getCurrentNavigation().extras.state.detailPost);
        } else {
          this.services.push(
            this.router.getCurrentNavigation().extras.state.data.group ||
              this.router.getCurrentNavigation().extras.state.data
          );
        }
      }
    });
  }

  ngOnInit() {}

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  openDetails(url: string, data) {
    const navigationExtras: NavigationExtras = {
      state: {
        detailPost: data,
      },
    };
    this.router.navigate([url], navigationExtras);
  }

  async openHelpModal() {
    const modal = await this.modalController.create({
      component: HelpPagePage,
      cssClass: '',
      componentProps: {
        service: null,
      },
    });
    return await modal.present();
  }
}
