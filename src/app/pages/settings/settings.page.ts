import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
const project = require('package.json');

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  constructor(public navCtrl: NavController) {}

  get version() {
    return `${project.version}`;
  }

  ngOnInit() {}
}
