import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IntroPageRoutingModule } from './intro-routing.module';

import { IntroPage } from './intro.page';
import { TranslateModule } from '@ngx-translate/core';
import { SwiperModule } from 'swiper/angular';


@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, IntroPageRoutingModule, TranslateModule, SwiperModule],
  declarations: [IntroPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class IntroPageModule {
}
