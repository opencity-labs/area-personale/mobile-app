import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { LoadingController, ModalController } from '@ionic/angular';
import moment from 'moment';
import { CalendarService } from '../../services/calendar.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-cal-modal',
  templateUrl: './cal-modal.page.html',
  styleUrls: ['./cal-modal.page.scss'],
})
export class CalModalPage implements OnInit, AfterViewInit {
  @Input() timeSelected: any;
  @Input() calendarId: string;
  @Input() openingHours: string;

  calendar = {
    mode: 'month',
    currentDate: new Date(),
  };
  viewTitle: string;

  event = {
    title: '',
    desc: '',
    startTime: null,
    endTime: '',
    allDay: true,
  };

  modalReady = false;
  listTime = [];
  selectedDay: string;
  currentDate;
  private loading: any;
  selectedAtIndex: number;

  constructor(
    private modalCtrl: ModalController,
    private calendarService: CalendarService,
    public loadingController: LoadingController,
    public translateService: TranslateService
  ) {
    moment.locale(this.translateService.getDefaultLang());
  }

  ngOnInit() {
    this.presentLoading();
  }

  ngAfterViewInit() {
    this.currentDate = moment(this.timeSelected.selectedTime).format('YYYY-MM-DD');
    this.getAvailabilities(this.currentDate);

    setTimeout(() => {
      this.modalReady = true;
    }, 0);
  }

  save(el: any) {
    this.modalCtrl.dismiss({ event: el });
  }

  onViewTitleChanged(title) {
    this.viewTitle = title;
  }

  onTimeSelected(ev) {
    this.event.startTime = new Date(ev.selectedTime);
  }

  close() {
    this.modalCtrl.dismiss();
  }
  nextDay() {
    this.currentDate = moment(this.currentDate).add('1', 'days').format('YYYY-MM-DD');
    this.getAvailabilities(this.currentDate);
  }

  prevDay() {
    this.currentDate = moment(this.currentDate).add('-1', 'days').format('YYYY-MM-DD');
    this.getAvailabilities(this.currentDate);
  }

  getAvailabilities(date: any) {
    this.calendarService.getCalendarTimeAvailabilities(this.calendarId, date, this.openingHours).subscribe(
      (el) => {
        this.listTime = el;
      },
      (error) => {},
      () => {
        this.selectedDay = moment(this.currentDate).format('LL');
        this.loading.dismiss();
      }
    );
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      cssClass: '',
      message: this.translateService.instant('common.recupero_dati'),
    });
    await this.loading.present();
  }

  trackById(index, item) {
    return item.id;
  }

  selectedTime(i: number) {
    this.selectedAtIndex = i;
  }
}
