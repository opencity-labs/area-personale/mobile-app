import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-description-service',
  templateUrl: './description-service.page.html',
  styleUrls: ['./description-service.page.scss'],
})
export class DescriptionServicePage implements OnInit {
  @Input() service: any;
  constructor(public modalController: ModalController) {}

  ngOnInit() {}

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      dismissed: true,
    });
  }
}
