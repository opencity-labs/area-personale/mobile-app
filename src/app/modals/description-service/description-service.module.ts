import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DescriptionServicePageRoutingModule } from './description-service-routing.module';

import { DescriptionServicePage } from './description-service.page';
import { SharedPipesModule } from '../../pipe/shared-pipes.module';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, DescriptionServicePageRoutingModule, SharedPipesModule],
  declarations: [DescriptionServicePage],
})
export class DescriptionServicePageModule {}
