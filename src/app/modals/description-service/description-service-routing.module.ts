import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DescriptionServicePage } from './description-service.page';

const routes: Routes = [
  {
    path: '',
    component: DescriptionServicePage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DescriptionServicePageRoutingModule {}
