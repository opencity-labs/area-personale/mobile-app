import { Component, OnDestroy } from '@angular/core';
import { ModalController, Platform } from '@ionic/angular';
import { SplashScreen } from '@capacitor/splash-screen';
import { ServicesService } from './services/services.service';
import { StorageService } from './services/storage.service';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { LockedPage } from './pages/locked/locked.page';
import { UpdateService } from './services/update.service';
import { takeUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnDestroy {
  private unsubscribe$ = new Subject<void>();
  private modal: HTMLIonModalElement;

  constructor(
    private platform: Platform,
    private servicesService: ServicesService,
    private storageService: StorageService,
    private router: Router,
    public modalCtrl: ModalController,
    private updateService: UpdateService,
    private translate: TranslateService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.translate.setDefaultLang('it');
    this.platform.ready().then(async () => {
      await SplashScreen.hide();

      if (this.platform.is('ios')) {
        this.platform.resume.pipe(takeUntil(this.unsubscribe$)).subscribe(async () => {
          return await this.lockApp();
        });
        this.platform.pause.pipe(takeUntil(this.unsubscribe$)).subscribe(async () => {
          return this.modal.dismiss();
        });
      }
      await this.lockApp();
      await this.updateService.checkForUpdate();
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  lockApp() {
    return this.storageService.getKeys().then((res) => {
      if (res) {
        if (res.value.includes('BIOMETRIC_ACCESS')) {
          this.storageService.get('BIOMETRIC_ACCESS').then((res: any) => {
            if (res.value === 'true') {
              return this.generateModalSecurity();
            }
          });
        }
        if (res.value.includes('LANGUAGE')) {
          this.storageService.get('LANGUAGE').then((res: any) => {
            if (res.value) {
              this.translate.setDefaultLang(res.value.toString());
            }
          });
        }
      }
    });
  }

  async generateModalSecurity() {
    this.modal = await this.modalCtrl.create({
      component: LockedPage,
    });
    return await this.modal.present();
  }

  ionViewWillLeave() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
