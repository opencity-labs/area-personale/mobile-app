import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { FormPage } from './pages/form/form.page';
import { FormioSummaryPage } from './pages/formio-summary/formio-summary.page';
import { PraticeFormPage } from './pages/pratice-form/pratice-form.page';
import { IntroGuard } from './guards/intro.guard';
import { AutoLoginGuard } from './guards/auto-login.guard';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: 'tabs',
    loadChildren: () => import('./pages/tabs/tabs.module').then((m) => m.TabsPageModule),
    //canLoad: [AuthGuard], // Secure all child pages
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then((m) => m.LoginPageModule),
    canLoad: [IntroGuard, AutoLoginGuard], // Check if we should show the introduction or forward to inside
  },
  {
    path: 'settings',
    loadChildren: () => import('./pages/settings/settings.module').then((m) => m.SettingsPageModule),
  },
  {
    path: 'privacy',
    loadChildren: () => import('./pages/privacy/privacy.module').then((m) => m.PrivacyPageModule),
  },
  {
    path: 'form',
    component: FormPage,
    // loadChildren: () => import('./pages/form/form.module').then((m) => m.FormPageModule),
  },
  {
    path: 'service-details',
    loadChildren: () =>
      import('./pages/service-details/service-details.module').then((m) => m.ServiceDetailsPageModule),
  },
  {
    path: 'services',
    loadChildren: () => import('./pages/services/services.module').then((m) => m.ServicesPageModule),
  },
  {
    path: 'services/:topics_id',
    loadChildren: () => import('./pages/services/services.module').then((m) => m.ServicesPageModule),
  },
  {
    path: 'formio-summary',
    component: FormioSummaryPage,
    // loadChildren: () => import('./pages/formio-summary/formio-summary.module').then((m) => m.FormioSummaryPageModule),
  },
  {
    path: 'cal-modal',
    loadChildren: () => import('./modals/cal-modal/cal-modal.module').then((m) => m.CalModalPageModule),
  },
  {
    path: 'practices',
    loadChildren: () => import('./pages/practices/practices.module').then((m) => m.PracticesPageModule),
  },
  {
    path: 'details',
    loadChildren: () =>
      import('./pages/profile-details/profile-details.module').then((m) => m.ProfileDetailsPageModule),
  },
  {
    path: 'user',
    loadChildren: () => import('./pages/profile-details/user/user.module').then((m) => m.UserPageModule),
  },
  {
    path: 'contacts',
    loadChildren: () => import('./pages/profile-details/contacts/contacts.module').then((m) => m.ContactsPageModule),
  },
  {
    path: 'residence',
    loadChildren: () => import('./pages/profile-details/residence/residence.module').then((m) => m.ResidencePageModule),
  },
  {
    path: 'domicile',
    loadChildren: () => import('./pages/profile-details/domicile/domicile.module').then((m) => m.DomicilePageModule),
  },
  {
    path: 'document',
    loadChildren: () => import('./pages/profile-details/document/document.module').then((m) => m.DocumentPageModule),
  },
  {
    path: 'description-service',
    loadChildren: () =>
      import('./modals/description-service/description-service.module').then((m) => m.DescriptionServicePageModule),
  },
  {
    path: 'formio-complete',
    loadChildren: () =>
      import('./pages/formio-complete/formio-complete.module').then((m) => m.FormioCompletePageModule),
  },
  {
    path: 'pratice/:id',
    loadChildren: () =>
      import('./pages/pratice-details/pratice-details.module').then((m) => m.PraticeDetailsPageModule),
  },
  {
    path: 'pratice/:id/attachments',
    loadChildren: () =>
      import('./pages/pratices-attachments/pratices-attachments.module').then((m) => m.PraticesAttachmentsPageModule),
  },
  {
    path: 'pratice/:id/messages',
    loadChildren: () =>
      import('./pages/pratices-messages/pratices-messages.module').then((m) => m.PraticesMessagesPageModule),
  },
  {
    path: 'tenants',
    loadChildren: () => import('./pages/tenants/tenants.module').then((m) => m.TenantsPageModule),
  },
  {
    path: 'error-page',
    loadChildren: () => import('./pages/error-page/error-page.module').then((m) => m.ErrorPagePageModule),
  },
  {
    path: 'pratice/:id/form',
    component: PraticeFormPage,
    // loadChildren: () => import('./pages/pratice-form/pratice-form.module').then( m => m.PraticeFormPageModule)
  },
  {
    path: 'intro',
    loadChildren: () => import('./pages/intro/intro.module').then((m) => m.IntroPageModule),
  },
  {
    path: 'locked',
    loadChildren: () => import('./pages/locked/locked.module').then((m) => m.LockedPageModule),
  },
  {
    path: 'help-page',
    loadChildren: () => import('./modals/help-page/help-page.module').then((m) => m.HelpPagePageModule),
  },
  {
    path: 'sub-topics/:id',
    loadChildren: () => import('./pages/sub-topics/sub-topics.module').then((m) => m.SubTopicsPageModule),
  },
  {
    path: 'groups/:id',
    loadChildren: () => import('./pages/group-details/group-details.module').then((m) => m.GroupDetailsPageModule),
  },
  {
    path: 'meetings',
    loadChildren: () => import('./pages/meetings/meetings.module').then((m) => m.MeetingsPageModule),
  },
  {
    path: 'not-found-page',
    loadChildren: () => import('./pages/not-found-page/not-found-page.module').then((m) => m.NotFoundPagePageModule),
  },
  {
    path: 'security',
    loadChildren: () => import('./pages/security/security.module').then((m) => m.SecurityPageModule),
  },
  {
    path: 'preferences',
    loadChildren: () => import('./pages/preferences/preferences.module').then((m) => m.PreferencesPageModule),
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full',
  },
  { path: '**', redirectTo: '/not-found-page' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules
}),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
