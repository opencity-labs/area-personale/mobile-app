import { Injectable } from '@angular/core';
import { CanLoad, Router } from '@angular/router';
import { StorageService } from '../services/storage.service';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root',
})
export class IntroGuard implements CanLoad {
  constructor(private router: Router, private storage: StorageService, private authService: AuthService) {}

  async canLoad(): Promise<any> {
    await this.storage
      .getIntro()
      .then((hasSeenIntro) => {
        return true;
      })
      .catch((err) => {
        this.router.navigateByUrl('/intro', { replaceUrl: true });
        return false;
      });
  }
}
