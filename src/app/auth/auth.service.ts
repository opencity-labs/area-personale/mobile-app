import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { StorageService } from '../services/storage.service';

const TOKEN_KEY = 'ACCESS_TOKEN';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  // Init with null to filter out the first value in a guard!
  isAuthenticated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);

  constructor(private httpClient: HttpClient, private storage: StorageService) {
    this.loadToken();
  }

  async loadToken() {
    await this.storage
      .get(TOKEN_KEY)
      .then((token) => {
        if (token) {
          this.isAuthenticated.next(true);
        } else {
          this.isAuthenticated.next(false);
        }
      })
      .catch(() => {
        this.isAuthenticated.next(false);
      });
  }

  async logout() {
    await this.storage.removeToken();
    this.isAuthenticated.next(false);
  }

  getToken() {
    return this.storage.getToken();
  }
}
