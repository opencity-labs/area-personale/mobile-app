import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ApplicationDataConfig, ApplicationsConfig, Messages } from '../interfaces/applications';

@Injectable({
  providedIn: 'root',
})
export class ApplicationsService {
  constructor(private httpClient: HttpClient) {}

  getFormIOService(url: string) {
    return this.httpClient.get<any>(url);
  }

  postApplications(data: any) {
    return this.httpClient.post(`${environment.api_stanza}/api/applications`, data);
  }

  getApplications(order?: string, limit?: string, sort?: string) {
    const params = new HttpParams().set('order', order).set('limit', limit).set('sort', sort);
    return this.httpClient.get<ApplicationsConfig>(`${environment.api_stanza}/api/applications`, { params });
  }

  getApplicationsUrl(url: string) {
    return this.httpClient.get<ApplicationsConfig>(url);
  }

  getApplicationById(id: string) {
    return this.httpClient.get<ApplicationDataConfig>(`${environment.api_stanza}/api/applications/${id}`);
  }

  getApplicationsMessages(id: string) {
    return this.httpClient.get<Messages[]>(`${environment.api_stanza}/api/applications/${id}/messages`);
  }

  getApplicationsAttachments(id: string, idAtt: string) {
    return this.httpClient.get<Messages[]>(`${environment.api_stanza}/api/applications/${id}/attachments/${idAtt}`);
  }
}
