import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class GeographicAreasService {
  constructor(private httpClient: HttpClient) {}

  getGeographicAreas(): Observable<any> {
    return this.httpClient.get(`${environment.api_stanza}/api/geographic-areas`);
  }
}
