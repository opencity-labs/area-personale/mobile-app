import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AlertController, Platform } from '@ionic/angular';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { environment } from '../../environments/environment';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { AppUpdate } from '../interfaces/update-app';

@Injectable({
  providedIn: 'root',
})
export class UpdateService implements OnDestroy {
  updateExample = environment.metadata + '/app-tribunali/version.json';
  maintenanceExample = environment.metadata + '/app-tribunali/maintenance.json';
  private unsubscribe$ = new Subject<void>();

  constructor(
    private http: HttpClient,
    private alertCtrl: AlertController,
    private appVersion: AppVersion,
    private iab: InAppBrowser,
    private plt: Platform
  ) {}

  async checkForUpdate() {
    this.http
      .get(this.updateExample)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(async (info: AppUpdate) => {
        if (!info.enabled) {
          // App should be disabled right now
          this.presentAlert(info.msg.title, info.msg.msg);
        } else {
          // Compare major/minor versions and show according info
          const versionNumber = await this.appVersion.getVersionNumber();
          const splittedVersion = versionNumber.split('.');
          const serverVersion = info.current.split('.');
          // Check Major code, then minor code
          if (serverVersion[0] > splittedVersion[0]) {
            this.presentAlert(info.majorMsg.title, info.majorMsg.msg, info.majorMsg.btn);
          } else if (serverVersion[1] > splittedVersion[1]) {
            this.presentAlert(info.minorMsg.title, info.minorMsg.msg, info.minorMsg.btn, true);
          } else if (serverVersion[2] > splittedVersion[2]) {
            this.presentAlert(info.minorMsg.title, info.minorMsg.msg, info.minorMsg.btn, true);
          }
        }
      });
  }

  openAppstoreEntry() {
    // Use your apps bundle ID and iOS ID!
    if (this.plt.is('android')) {
      window.location.assign('https://play.google.com/store/apps/details?id=com.carClient.bookMyDreamCar');
    } else {
      this.iab.create(
        'itms-apps://itunes.apple.com/app/id1469563885',
        '_blank',
        'hidden=no,location=yes,clearsessioncache=yes,clearcache=yes,hidenavigationbuttons=yes,hideurlbar=yes,footer=no,toolbar=yes'
      );
    }
  }

  async presentAlert(header, message, buttonText = '', allowClose = false) {
    const buttons: any = [];
    if (buttonText !== '') {
      /*buttons.push({
        text: buttonText,
        handler: () => {
          this.openAppstoreEntry();
        }
      })*/
    }

    if (allowClose) {
      buttons.push({
        text: 'Chiudi',
        role: 'cancel',
      });
    }

    const alert = await this.alertCtrl.create({
      header,
      message,
      buttons,
      backdropDismiss: allowClose,
    });

    await alert.present();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
