import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class RecipientsService {
  constructor(private httpClient: HttpClient) {}

  getRecipients(): Observable<any> {
    return this.httpClient.get(`${environment.api_stanza}/api/recipients`);
  }
}
