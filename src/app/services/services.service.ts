import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/internal/Observable';
import { ServiceConfig } from '../interfaces/services';

@Injectable({
  providedIn: 'root',
})
export class ServicesService {
  constructor(private httpClient: HttpClient) {}

  getAllServices(
    topics_id?: string,
    service_group_id?: string,
    grouped?: boolean,
    recipient_id?: string,
    geographic_area_id?: string
  ): Observable<any> {
    let params = new HttpParams();
    if (topics_id) params = params.append('topics_id', topics_id ? topics_id : '');

    if (service_group_id) params = params.append('service_group_id', service_group_id ? service_group_id : '');

    if (grouped) params = params.append('grouped', grouped ? grouped : false);

    if (recipient_id) {
      params = params.append('recipient_id', recipient_id ? recipient_id : '');
    }

    if (geographic_area_id) params = params.append('geographic_area_id', geographic_area_id ? geographic_area_id : '');


    return this.httpClient.get(`${environment.api_stanza}/api/services`, { params });
  }

  getServicesUrlParameters(params: string): Observable<any> {
    return this.httpClient.get(`${environment.api_stanza}/api/services?${params}`);
  }

  getCategories(): Observable<any> {
    return this.httpClient.get(`${environment.api_stanza}/api/categories`);
  }

  getCategoryById(id: string): Observable<any> {
    return this.httpClient.get(`${environment.api_stanza}/api/categories/${id}`);
  }

  getServiceByUrl(url: string): Observable<any> {
    return this.httpClient.get(url);
  }

  getServices(): Observable<any> {
    return this.httpClient.get(`${environment.api_stanza}/api/services`);
  }

  postService(data: any): Observable<any> {
    return this.httpClient.post(`${environment.api_stanza}/api/services`, data);
  }

  getGroups(
    topics_id?: string,
    recipient_id?: string,
    geographic_area_id?: string,
    not_empty?: string
  ): Observable<any> {
    let params = new HttpParams();
    if (topics_id) params = params.append('topics_id', topics_id ? topics_id : '');

    if (recipient_id) {
      params = params.append('recipient_id', recipient_id ? recipient_id : '');
    }

    if (geographic_area_id) params = params.append('geographic_area_id', geographic_area_id ? geographic_area_id : '');

    if (not_empty) params = params.append('not_empty', not_empty ? not_empty : false);

    return this.httpClient.get(`${environment.api_stanza}/api/services-groups`, { params });
  }

  getGroupsById(id: string): Observable<any> {
    return this.httpClient.get(`${environment.api_stanza}/api/services-groups/${id}`);
  }

  getServiceById(id: string) {
    return this.httpClient.get<ServiceConfig>(`${environment.api_stanza}/api/services/${id}`);
  }

  getFacets() {
    return this.httpClient.get<any>(`${environment.api_stanza}/api/services/facets`);
  }
}
