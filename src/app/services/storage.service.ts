import { Injectable } from '@angular/core';
import { SecureStoragePlugin } from 'capacitor-secure-storage-plugin';
import jwt_decode from 'jwt-decode';
import { Storage } from '@capacitor/storage';

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  constructor() {}

  public set(key: string, value: any) {
    return SecureStoragePlugin.set({ key, value });
  }
  public get(key: string) {
    return SecureStoragePlugin.get({ key });
  }
  public getToken() {
    return SecureStoragePlugin.get({ key: 'ACCESS_TOKEN' });
  }

  public getKeys() {
    return SecureStoragePlugin.keys();
  }

  public getIntro() {
    return SecureStoragePlugin.get({ key: 'INTRO_SEEN' });
  }

  public setIntro(value: string = 'false') {
    return SecureStoragePlugin.set({ key: 'INTRO_SEEN', value });
  }

  public setCategories(value: any) {
    return SecureStoragePlugin.set({ key: 'CATEGORIES', value });
  }
  public setLanguages(value: any) {
    return SecureStoragePlugin.set({ key: 'LANGUAGE', value });
  }
  public setBiometrics(value: any) {
    return SecureStoragePlugin.set({ key: 'BIOMETRIC_ACCESS', value });
  }

  public removeToken() {
    return SecureStoragePlugin.remove({ key: 'ACCESS_TOKEN' });
  }

  public clearStorage() {
    return SecureStoragePlugin.clear();
  }

  public decodeUser(): Promise<any> {
    return this.getToken()
      .then((res) => {
        return jwt_decode(res.value);
      })
      .catch((err) => {
        console.error(err);
      });
  }

  public remove(key) {
    return SecureStoragePlugin.remove({ key });
  }
}
