import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { StorageService } from './storage.service';
import Dictionary from '../utility/dictionary';
import { environment } from '../../environments/environment';
import { Categories } from '../interfaces/categories';

@Injectable({
  providedIn: 'root',
})
export class CategoriesService {
  private dict: Dictionary;
  constructor(private httpClient: HttpClient, private storageService: StorageService) {
    this.dict = new Dictionary();
    this.storageService
      .get('CATEGORIES')
      .then((res) => {
        const category = JSON.parse(res.value);
        category.forEach((el) => {
          this.dict.set(el.id, el);
        });
      })
      .catch((err) => {
        console.error(err);
      });
  }

  getCategoryDictionaryById(id: string) {
    return this.dict.get(id);
  }

  getCategoryById(id: string) {
    return this.httpClient.get<Categories>(`${environment.api_stanza}/api/categories/${id}`);
  }

  getCategories(not_empty?: string) {
    let params = new HttpParams();
    if (not_empty) params = params.append('not_empty', not_empty ? not_empty : false);

    return this.httpClient.get<Categories[]>(`${environment.api_stanza}/api/categories`, { params });
  }
}
