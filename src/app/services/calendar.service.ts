import { Inject, Injectable, LOCALE_ID } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CalendarService {
  constructor(private httpClient: HttpClient, @Inject(LOCALE_ID) private locale: string) {}

  getCalendar(calendarId: string) {
    return this.httpClient.get<any>(`${environment.api_stanza}/api/calendars/${calendarId}`);
  }

  getCalendarAvailabilities(calendarId: string) {
    return this.httpClient.get<any[]>(`${environment.api_stanza}/api/calendars/${calendarId}/availabilities`);
  }

  getCalendarTimeAvailabilities(calendarId: string, date: string, opening_hours?: string) {
    const openingHours = opening_hours ? `?opening_hours=${opening_hours}` : ''
    return this.httpClient.get<any[]>(`${environment.api_stanza}/api/calendars/${calendarId}/availabilities/${date}${openingHours}`);
  }

  createDraftCalendarMeeting(data: any) {
    return this.httpClient.post<any>(`${environment.api_stanza}/it/meetings/new-draft`, data);
  }
}
