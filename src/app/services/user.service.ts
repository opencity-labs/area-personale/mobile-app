import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from '../../environments/environment';
import { StorageService } from './storage.service';
import { User } from '../interfaces/user';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private httpClient: HttpClient, private storageService: StorageService) {}

  getUser(id: string): Observable<any> {
    return this.httpClient.get(`${environment.api_stanza}/api/users/${id}`);
  }

  patchUser(id: string, data: User): Observable<any> {
    return this.httpClient.patch(`${environment.api_stanza}/api/users/${id}`, data);
  }

  putUser(id: string, data: User): Observable<any> {
    return this.httpClient.put(`${environment.api_stanza}/api/users/${id}`, data);
  }

  getUserByFiscalCode(fiscalCode: string): Observable<any> {
    return this.httpClient.get(`${environment.api_stanza}/api/users/${fiscalCode}`);
  }

  getUsers(): Observable<any> {
    return this.httpClient.get(`${environment.api_stanza}/api/users`);
  }

  getProfile(): Observable<any> {
    return this.httpClient.get(`${environment.api_stanza}/api/user/profile`);
  }
}
