import { FormioAppConfig } from '@formio/angular';
import { FormioAuthConfig } from '@formio/angular/auth';

export const AppConfig: FormioAppConfig = {
  appUrl: 'https://form.stanzadelcittadino.it',
  apiUrl: 'https://form-prod.ship.opencontent.io',
  icons: 'fontawesome',
};

export const AuthConfig: FormioAuthConfig = {
  login: {
    form: 'user/login',
  },
  register: {
    form: 'user/register',
  },
};
