import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from '../auth/auth.service';
import { StorageService } from '../services/storage.service';
import { Router } from '@angular/router';

export class ErrorInterceptor implements HttpInterceptor {
  constructor(public authService: AuthService, public storageService: StorageService, public router: Router) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.error instanceof ErrorEvent) {
          console.log(`Error: ${error.error.message}`);
        } else {
          console.log(`Error Code: ${error.status},  Message: ${error.message}`);
        }
        if (error.status === 401) {
          this.storageService.removeToken().then((r) => {
            alert('Sessione scaduta');
            this.authService.isAuthenticated.next(false);
            this.router.navigateByUrl('/login');
          });
        } else if (error.status === 500) {
          this.router.navigate(['error-page']);
        } else if (error.status === 404) {
          if (!this.router.url.includes('pratice')) {
            this.router.navigate(['not-found-page']);
          }
        }
        return throwError(error);
      })
    );
  }
}
